/**
 * Created by Redfox on 20.08.2015.
 */
const gulp = require('gulp'),
  browserify = require('browserify'),
  source = require('vinyl-source-stream');

gulp.task('browserify-main', function () {
  // place code for your default task here
  // set up the browserify instance on a task basis
  var b = browserify({
    entries: './renderer_process/js/client/client.js',
    debug: true
  });

  return b.bundle()
    .pipe(source('client-bundle.js'))
    .pipe(gulp.dest('./renderer_process/js/lib'));
});

gulp.task('browserify-tests', function () {
  // place code for your default task here
  // set up the browserify instance on a task basis
  var b = browserify({
    entries: './main_process/server/public/js/tests.js',
    debug: true
  });

  return b.bundle()
    .pipe(source('tests-bundle.js'))
    .pipe(gulp.dest('./main_process/server/public/js'));
});

gulp.task('default', ['browserify-main', 'browserify-tests']);
