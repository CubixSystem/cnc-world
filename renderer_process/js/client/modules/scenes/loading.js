/**
 * Created by Redfox on 14.11.2015.
 */
const Crafty = require('craftyjs');
const graphicsLoader = require('../graphics-loader');

const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;

const groundBlock = require('../components/ground-block/ground-block');

const defineScene = () => {
  'use strict';
  Crafty.defineScene(
    'Loading',
    function init() {
      'use strict';

      let gameCanvas = document.getElementById('game');
      Crafty.init(window.innerWidth, window.innerHeight, gameCanvas);

      Crafty.timer.FPS(60);

      ipcRenderer.on('blocksConfig-reply', (event, data) => {
        console.log('blocksConfig', data);
        groundBlock.createComponent(data);
      });
      ipcRenderer.send('blocksConfig-message');

      console.time('graphicsLoader');
      graphicsLoader.load()
        .then(function then() {
          console.timeEnd('graphicsLoader');
          Crafty.enterScene('Main menu');
        });
    },
    function uninit() {
      'use strict';

    });
};


module.exports.defineScene = defineScene;
