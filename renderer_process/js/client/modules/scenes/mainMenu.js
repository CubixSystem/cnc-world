/**
 * Created by Redfox on 14.11.2015.
 */
const Crafty = require('craftyjs');
const uuid = require('node-uuid');

const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;

const qr = require('qr-image');

const defineScene = () => {
  Crafty.defineScene(
    'Main menu',
    function init() {
      'use strict';

      //helloWorldText.text('ZENITH')
      ipcRenderer.on('New extraClient', (event, data) => {
        console.log('New extraClient', data);
        let helloWorldText = Crafty.e('2D, DOM, Text')
          .attr({
            x: 100,
            y: 10,
            w: 1000,
            h: 50
          })
          .text('New connection: ' + data.uuid)
          .textFont({
            family: 'OCRAStd',
            size: '15pt'
          })
          .textColor('#FF6F59')
          .unselectable();
        console.log(helloWorldText.text());
      });
/*      ipcRenderer.send('WorldId', {
        uuid: helloWorldText.text()
      });*/

      let qrButton = Crafty.e('Button')
        .setCallbacks(
          function onMouseClick() {
            
            let newUuid = uuid.v4();
            ipcRenderer.send('WaitNewConnection', {
              uuid: newUuid
            });
            
            //Generate qr
            let qrGenerator = (qrMatrix, scaleFactor) => {
              let qrImgWidth = qrMatrix.length * scaleFactor,
                qrImgHeight = qrMatrix.length * scaleFactor,
                buffer = new Uint8ClampedArray(qrImgWidth * qrImgHeight * 4);

              let setPixelInUint8ClampedArray = (coords, color, buffer) => {
                let coordsToPos = (coords) => {
                  return (coords.y * qrImgWidth + coords.x) * 4;
                };
                buffer[coordsToPos(coords)] = color.r;
                buffer[coordsToPos(coords) + 1] = color.g;
                buffer[coordsToPos(coords) + 2] = color.b;
                buffer[coordsToPos(coords) + 3] = color.a;
              };

              for (let y = 0; y < qrMatrix.length; y++) {
                for (let x = 0; x < qrMatrix.length; x++) {
                  let pos = (y * scaleFactor * qrImgWidth + x * scaleFactor) * 4; // position in buffer based on x and y
                  if (qrMatrix[y][x] === 1) {
                    for (let i = 0; i < scaleFactor; i++) {
                      for (let j = 0; j < scaleFactor; j++) {
                        setPixelInUint8ClampedArray({x: x * scaleFactor + j, y: y * scaleFactor + i},
                          {r: 255, g: 183, b: 66, a: 255}, buffer);
                      }
                    }
                  } else {
                    buffer[pos] = 0;
                    buffer[pos + 1] = 0;
                    buffer[pos + 2] = 0;
                    buffer[pos + 3] = 0;
                  }
                }
              }
              let imageData = new ImageData(buffer, qrImgWidth, qrImgHeight);
              Crafty.asset('testQR.png', imageData);
              Crafty.sprite(imageData.width, imageData.height, 'testQR.png', {qr: [0, 0]});
              let qrEntity = Crafty.e('2D, WebGL, qr');
            };
            
            qrGenerator(qr.matrix('http://192.168.1.108:3000/' + newUuid), 3);
            
          },
          function onMouseOver() {
            qrButton.color('green');
          },
          function onMouseOut() {
            qrButton.color('#FF6F59');
          })
        .attr({
          x: 500,
          y: 200,
          w: 200,
          h: 100
        })
        .color('#FF6F59');

      let testButton = Crafty.e('Button')
        .attr({x:150, y:90, w:100, h:100})
        .append("<a href='index.html'>Index</a>");

      let menuButton = Crafty.e('Button')
        .setCallbacks(
          function onMouseClick() {
            Crafty.enterScene('World');
          },
          function onMouseOver() {
            menuButton.color('green');
          },
          function onMouseOut() {
            menuButton.color('#FF6F59');
          })
        .attr({
          x: 200,
          y: 200,
          w: 200,
          h: 100
        })
        .color('#FF6F59');

    },
    function uninit() {
      'use strict';

    });
};

module.exports.defineScene = defineScene;
