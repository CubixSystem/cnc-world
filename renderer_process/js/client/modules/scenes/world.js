/**
 * Created by Redfox on 14.11.2015.
 */
const Crafty = require('craftyjs');
const unitModule = require('../components/unit/unit'),
  pathfindModule = require('../components/pathfind'),
  mapModule = require('../components/map/map'),
  gameInterfaceModule = require('../components/interface/interface');

const defineScene = () => {
  Crafty.defineScene(
    'World',
    function init(locals) {
      'use strict';
      let map = {
        size: {
          x: 20,
          y: 20
        },
        titleW: 96,
        titleH: 48
      };
      map.width = map.size.x * map.titleW;
      map.height = map.size.y * map.titleH;

      let iso = Crafty.diamondIso.init(map.titleW, map.titleH, map.size.x, map.size.y, map.width / 2, 0);

      pathfindModule.createPathfind(map);
      mapModule.createMap(iso, map);
      unitModule.createUnit(iso, {x: 0, y: 0, z: 1});
      //unitModule.createUnit(iso, {x: 4, y: 5, z: 1});

      gameInterfaceModule.createInterface();

    },
    function uninit() {
      'use strict';

    });
};

module.exports.defineScene = defineScene;
