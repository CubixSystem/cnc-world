/**
 * Created by Redfox on 14.11.2015.
 */

const loadingScene = require('./loading'),
  mainMenuScene = require('./mainMenu'),
  world = require('./world');

const init = () => {
  'use strict';
  loadingScene.defineScene();
  mainMenuScene.defineScene();
  world.defineScene();
};

module.exports.init = init;

