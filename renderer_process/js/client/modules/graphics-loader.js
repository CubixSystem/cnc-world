/**
 * Created by Redfox on 16.08.2015.
 */
const Crafty = require('craftyjs');

const loadSingleSprite = (assetsObj) => {
  'use strict';
  return new Promise(function promise(resolve, reject) {
    Crafty.load(assetsObj, function load() {
      resolve();
    });
  });
};

const load = () => {
  return new Promise(function promise(resolve, reject) {
    'use strict';

    let blocksSprites = {
      'sprites': {}
    };
    blocksSprites.sprites['../renderer_process/img/sprites/' + 'sprites.png'] = {
      // This is the width of each image in pixels
      tile: 96,
      // The height of each image
      tileh: 96,
      // We give names to three individual images
      map: {
        flat: [0, 0],
        backLeftUp: [1, 0],
        backRightUp: [2, 0],
        frontLeftUp: [3, 0],
        frontRightUp: [4, 0],
        leftUp: [0, 1],
        rightUp: [1, 1],
        frontUp: [2, 1],
        backUp: [3, 1],
        middleLeftUp: [0, 2],
        middleRightUp: [1, 2],
        middleFrontUp: [2, 2],
        middleBackUp: [3, 2]
      }
    };

    let botSprites = {
      'sprites': {
        '../renderer_process/img/sprites/bot.png': {
          // This is the width of each image in pixels
          tile: 96,
          // The height of each image
          tileh: 128,
          // We give names to three individual images
          map: {
            bot_Left: [0, 0],
            bot_LeftTop: [1, 0],
            bot_Top: [2, 0],
            bot_RightTop: [3, 0],
            bot_Right: [4, 0],
            bot_RightDown: [0, 1],
            bot_Down: [1, 1],
            bot_LeftDown: [2, 1]
          }
        }
      }
    };

    let grassFlat = {
      'sprites': {
        '../renderer_process/img/sprites/grassFlat.png': {
          // This is the width of each image in pixels
          tile: 96,
          // The height of each image
          tileh: 96,
          // We give names to three individual images
          map: {
            grass_1: [0, 0],
            flower_1: [1, 0],
            grass_2: [2, 0],
            grass_3: [3, 0],
            flower_2: [4, 0],
            grass_4: [5, 0],
            empty: [6, 0]
          }
        }
      }
    };

/*    var testSprite = {
      'sprites': {
        'img/sprites/landscapeTiles_067.png': {
          // This is the width of each image in pixels
          tile: 64,
          // The height of each image
          tileh: 64,
          // We give names to three individual images
          map: {
            ground: [0, 0]
          }
        }
      }
    };

    var testSprite2 = {
      'sprites': {
        'img/sprites/landscapeTiles_098.png': {
          // This is the width of each image in pixels
          tile: 64,
          // The height of each image
          tileh: 64,
          // We give names to three individual images
          map: {
            ground2: [0, 0]
          }
        }
      }
    };*/


    Promise.all([
      loadSingleSprite(blocksSprites),
      loadSingleSprite(botSprites),
      loadSingleSprite(grassFlat)
/*      loadSingleSprite(testSprite),
      loadSingleSprite(testSprite2)*/
    ])
      .then(function then() {
        console.log('~~All graphics loaded!~~');
        resolve();
      })
      .catch(function catchErr(err) {
        reject(err);
      });
  });
};

module.exports.load = load;
