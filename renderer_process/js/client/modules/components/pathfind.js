/**
 * Created by Redfox on 17.08.2015.
 */
const Crafty = require('craftyjs');
const EasyStar = require('easystarjs');
const easystar = new EasyStar.js();

var createComponent = function createComponent() {
  'use strict';
  
  Crafty.c('PathfindModule', {
    _eventsToListen: {
      'PathfindModule:FindPath': 'PathfindModule:FindPath',
      'PathfindModule:MoveUnit': 'PathfindModule:MoveUnit',
      'PathfindModule:SetBlockWalkable': 'PathfindModule:SetBlockWalkable',
      'PathfindModule:GetBlockWalkable': 'PathfindModule:GetBlockWalkable'
    },
    _eventsToEmit: {
      'PathfindModule:PathfindModule': 'PathfindModule:PathNotFound',
      'PathfindModule:PathReady': 'PathfindModule:PathReady',
      'PathfindModule:GetBlockWalkableResponse': 'PathfindModule:GetBlockWalkableResponse'
    },
    init: function init() {

      this.grid = [];
      
      this.bind(this._eventsToListen['PathfindModule:FindPath'], function findPath(msg) {
        var self = this;
        easystar.findPath(
          msg.startPointIsoCoord.x,
          msg.startPointIsoCoord.y,
          msg.endPointIsoCoord.x,
          msg.endPointIsoCoord.y,
          function pathFound(path) {
            if (path === null) {
              Crafty(msg.ownerInGameId).trigger(self._eventsToEmit['PathfindModule:PathNotFound']);
            } else {
              if (path.length != 0) {
                var pathFoundData = {
                  ownerInGameId: msg.ownerInGameId,
                  path: path,
                  pathReady: true,
                  stepNumber: 0,
                  startPoint: path[0],
                  nextPoint: path[1],
                  endPoint: path[length - 1]
                };
                //console.log(pathFoundData);
                Crafty(pathFoundData.ownerInGameId).trigger(self._eventsToEmit['PathfindModule:PathReady'], pathFoundData);
                //console.log('PathfindModule:PathReady');
              }
            }
          });
        easystar.calculate();
      });
      this.bind(this._eventsToListen['PathfindModule:SetBlockWalkable'], function setBlockWalkable(msg) {
        this.grid[msg.isoCoord.y][msg.isoCoord.x] = msg.value;
      });
      this.bind(this._eventsToListen['PathfindModule:GetBlockWalkable'], function getBlockWalkable(msg) {
        //console.log('PathfindModule:GetBlockWalkableResponse');
        Crafty(msg.ownerInGameId).trigger(this._eventsToEmit['PathfindModule:GetBlockWalkableResponse'], {
          ownerInGameId: msg.ownerInGameId,
          isoCoord: msg.isoCoord,
          value: this.grid[msg.isoCoord.y][msg.isoCoord.x]
        });
      });
      this.bind(this._eventsToListen['PathfindModule:MoveUnit'], function moveUnit(msg) {
        this.grid[msg.oldIsoCoord.y][msg.oldIsoCoord.x] = 0;
        this.grid[msg.newIsoCoord.y][msg.newIsoCoord.x] = 1;
        //console.log(this.grid);
      });
    },
    preparePathMap: function preparePathMap(map) {
      for (var i = 0; i < map.size.y; i++) {
        this.grid[i] = [];
        for (var j = 0; j < map.size.x; j++) {
          this.grid[i][j] = 0;
        }
      }

      easystar.setGrid(this.grid);
      easystar.setAcceptableTiles([0]);
      easystar.enableDiagonals();

      //easystar.disableCornerCutting();
      return this;
    }
  });
};
var createPathfind = function createPathfind(map) {
  'use strict';
  var pathfindModule = Crafty.e('PathfindModule')
    .preparePathMap(map);
};

createComponent();
module.exports.createPathfind = createPathfind;
