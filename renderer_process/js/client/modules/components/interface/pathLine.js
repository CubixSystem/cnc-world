/**
 * Created by Redfox on 06.09.2015.
 */

const Crafty = require('craftyjs');  
  
var createComponent = function createComponent() {
  'use strict';

  Crafty.c('PathLine', {
    init: function init() {
      this.requires('2D, WebGL, Color');

      this.ready = false;
      this._startPoint = {};
      this._endPoint = {};

      this.bind('Draw', this._drawMe);
    },
    _drawMe: function _drawMe(e) {

      //this.ctx = e.ctx;
      e.ctx.lineWidth = 1;
      e.ctx.strokeStyle = '#00A800';
      e.ctx.fillStyle = '#00A800';

      //e.ctx.translate(0.5, 0.5);

      e.ctx.fillRect(this._startPoint.x - 2, this._startPoint.y - 1, 4, 3);
      e.ctx.fillRect(this._endPoint.x - 2, this._endPoint.y - 1, 4, 3);
      
      e.ctx.beginPath();
      e.ctx.moveTo(this._startPoint.x - 0.5, this._startPoint.y + 0.5);
      e.ctx.lineTo(this._endPoint.x - 0.5, this._endPoint.y  + 0.5);
      e.ctx.stroke();
      e.ctx.closePath();

      return this;
    },
    drawLine: function drawLine(startPoint, endPoint) {
      this._startPoint.x = startPoint.x + 32;
      this._startPoint.y = startPoint.y + 64;
      this._endPoint.x = endPoint.x + 32;
      this._endPoint.y = endPoint.y - 32;

      this.x = Math.min(this._startPoint.x, this._endPoint.x) - 2;
      this.y = Math.min(this._startPoint.y, this._endPoint.y) - 1;
      this.w = Math.abs(this._startPoint.x - this._endPoint.x) + 4;
      this.h = Math.abs(this._startPoint.y - this._endPoint.y) + 3;
      this.ready = true;

      return this;
    }
  });

};

var createLine = function createLine(msg) {
  'use strict';

  return Crafty.e('PathLine')
    .addComponent('PathLine')
    //.color('white')
    .attr({
      z: 500000
    })
    .drawLine(msg.startPoint, msg.endPoint);
};

createComponent();
module.exports.createLine = createLine;
