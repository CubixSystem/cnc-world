/**
 * Created by Redfox on 27.10.2015.
 */

const Crafty = require('craftyjs');  

const createComponent = function () {
  'use strict';
  Crafty.c('Button', {
    init: function () {
      this.addComponent('2D, HTML, Mouse, Color');

    },
    setCallbacks: function setCallbacks(onMouseClick, onMouseOver, onMouseOut) {
      this.bind('MouseDown', function mouseDown(e) {
        switch (e.mouseButton) {
          case 0:
            onMouseClick();
            break;
        }
      });

      this.bind('MouseOver', function mouseOver(e) {
        onMouseOver();
      });

      this.bind('MouseOut', function mouseOut(e)  {
        onMouseOut();
      });
      return this;
    }
  });
};
//var createButton = (onMouseClick, onMouseOver, onMouseOut, attr) => {
//  'use strict';
//  Crafty.e('Button')
//    .setCallbacks(onMouseClick, onMouseOver, onMouseOut)
//    .attr({
//      x: 200,
//      y: 200,
//      w: 200,
//      h: 200
//    })
//    .color('#FF6F59');
//};

createComponent();
//module.exports.createButton = createButton;
