/**
 * Created by Redfox on 06.09.2015.
 */

const Crafty = require('craftyjs');

var drawEllipseWithBezierByCenter = function drawEllipseWithBezierByCenter(ctx, cx, cy, w, h, style) {
  drawEllipseWithBezier(ctx, cx - w / 2.0, cy - h / 2.0, w, h, style);
};
var drawEllipseWithBezier = function drawEllipseWithBezier(ctx, x, y, w, h, style) {
  var kappa = .5522848,
    ox = (w / 2) * kappa, // control point offset horizontal
    oy = (h / 2) * kappa, // control point offset vertical
    xe = x + w,           // x-end
    ye = y + h,           // y-end
    xm = x + w / 2,       // x-middle
    ym = y + h / 2;       // y-middle

  ctx.save();
  ctx.beginPath();
  ctx.moveTo(x, ym);
  ctx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
  ctx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
  ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
  ctx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
  if (style)
    ctx.strokeStyle = style;
  ctx.stroke();
  ctx.restore();
};

var createComponent = function createComponent() {
  'use strict';

  Crafty.c('Ellipse', {
    init: function init() {
      this.requires('2D, WebGL, Color');

      this.ready = false;
      this._startPoint = {};
      this._endPoint = {};

      this.bind('Draw', this._drawMe);
    },
    _drawMe: function _drawMe(e) {
      e.ctx.lineWidth = 1;
      e.ctx.strokeStyle = 'red';

      e.ctx.beginPath();
/*      e.ctx.moveTo(this._startPoint.x, this._startPoint.y);
      e.ctx.lineTo(this._startPoint.x + (256)/2 - 16, this._startPoint.y + (128)/2 - 8);*/
      
      this.xteststartPoint = this._startPoint.x + (256)/2 - 16;
      this.yteststartPoint = this._startPoint.y + (128)/2 - 8;

      this.p1x = this.xteststartPoint - 64 - 16;
      this.p1y = this.yteststartPoint + 32 + 8;

      this.p2x = this.xteststartPoint + 64 + 16;
      this.p2y = this.yteststartPoint - 32 - 8;

      this.p3x = this.p2x - 32;
      this.p3y = this.p2y - 16;

      this.p4x = this.p3x + 32;
      this.p4y = this.p3y - 16;
      
/*      e.ctx.moveTo(this.xteststartPoint, this.yteststartPoint);
      e.ctx.lineTo(this.xteststartPoint + 64, this.yteststartPoint - 32);*/
      //e.ctx.moveTo(this.xteststartPoint, this.yteststartPoint);
      e.ctx.moveTo(this.p1x, this.p1y);
      e.ctx.lineTo(this.p2x, this.p2y);
      e.ctx.lineTo(this.p3x, this.p3y);
      e.ctx.lineTo(this.p4x, this.p4y);

/*      e.ctx.moveTo(this._startPoint.x, this._startPoint.y);
      e.ctx.lineTo(this._startPoint.x - (256)/2, this._startPoint.y + (128)/2);

      e.ctx.moveTo(this._startPoint.x, this._startPoint.y);
      e.ctx.lineTo(this._startPoint.x + (256)/2, this._startPoint.y - (128)/2);

      e.ctx.moveTo(this._startPoint.x, this._startPoint.y);
      e.ctx.lineTo(this._startPoint.x - (256)/2, this._startPoint.y - (128)/2);*/
      
      e.ctx.stroke();
      e.ctx.closePath();

      drawEllipseWithBezierByCenter(e.ctx, this._startPoint.x, this._startPoint.y, 256 + 96, 128 + 64, '#0099ff');

      return this;
    },
    drawEllipse: function drawEllipse(startPoint) {
      this._startPoint.x = startPoint.left + 32;
      this._startPoint.y = startPoint.top + 64;
      /*      this._endPoint.x = endPoint.left + 32;
       this._endPoint.y = endPoint.top - 32;*/

      this.x = this._startPoint.x - (256 + 96) / 2 - 30;
      this.y = this._startPoint.y - (128 + 64) / 2 - 30;
      this.w = 256 + 96 + 60;
      this.h = 128 + 64 + 60;

      this.ready = true;

      return this;
    }
  });

};
var t;
var createEllipse = function createEllipse(startPoint) {
  'use strict';

  t = Crafty.e('Ellipse')
    .addComponent('Ellipse')
    //.color('white')
    .attr({
      z: 500000
    })
    .drawEllipse(startPoint);

};

var updateEllipse = function updateEllipse(startPoint) {
  'use strict';
  t.drawEllipse(startPoint);
};

var destroyEllipse = function destroyEllipse() {
  'use strict';
  t.destroy();
  //t.trigger('Invalidate');
  console.log('Ellipse: desrt');
  //console.log(t);
};

createComponent();
module.exports.createEllipse = createEllipse;
module.exports.updateEllipse = updateEllipse;
module.exports.destroyEllipse = destroyEllipse;
