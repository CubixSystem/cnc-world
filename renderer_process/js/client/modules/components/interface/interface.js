/**
 * Created by Redfox on 06.09.2015.
 */

const Crafty = require('craftyjs');

const pathLine = require('./pathLine');
const selectRect = require('./selectRect');
var ellipse = require('./ellipse');
var panel = require('./panel');
require('./panelButton');

var toDataURL = require('to-data-url');

var createComponent = function createComponent() {
  'use strict';

  Crafty.c('GameInterface', {
    init: function init() {
      this.requires('2D, WebGL');

      this.selectedUnits = [];
      this.pathLines = {};
      this.selectMode = 'single';
      this.clickMode = 'unitCommand';
      //this.clickMode = 'blockChange';

      this.viewportMoveX = 0;
      this.viewportMoveY = 0;

      this.bind('GameInterface:DrawPathLine', function drawPathLine(msg) {
        //console.log('DrawPathLine ', this.pathLines);

        this.pathLines[msg.id] = pathLine.createLine(msg);

        //ellipse.createEllipse(msg.startPoint);
      });
      this.bind('GameInterface:UpdatePathLine', function updatePathLine(msg) {
        //console.log('UpdatePathLine: ', this.pathLines);

        if (this.pathLines[msg.id]) {
          this.pathLines[msg.id].drawLine(msg.startPoint, msg.endPoint);
        }

        //ellipse.updateEllipse(msg.startPoint);
      });
      this.bind('GameInterface:DestroyPathLine', function destroyPathLine(id) {
        if (this.pathLines[id]) {
          this.pathLines[id].destroy();
          delete this.pathLines[id];
        } else {
          console.error('DestroyPathLine', this.pathLines, id);
        }

        //ellipse.destroyEllipse();
      });
      this.bind('GameInterface:DrawPanel', function drawPanel(msg) {
        panel.createPanel();
      });

      this.bind('GameInterface:UnitClicked', function unitClicked(msg) {
        if (this.selectMode === 'add') {
          Crafty(msg.id).trigger('GameInterface:SelectedByPlayer');
          this.selectedUnits.push(msg.id);
        } else {
          if (this.selectedUnits.length > 0) {
            this.selectedUnits.forEach(function (element) {
              Crafty(element).trigger('GameInterface:Deselect');
            });
            self.selectedUnits = [];
          }

          Crafty(msg.id).trigger('GameInterface:SelectedByPlayer');
          this.selectedUnits.push(msg.id);
        }
      });

      this.bind('GroundBlock:ClickedHere', function clickedHere(msg) {
        switch (this.clickMode) {
          case 'unitCommand':
            this.selectedUnits.forEach(function (element) {
              Crafty(element).trigger('GameInterface:MoveToPoint', msg);
            });
            break;
          case 'blockChange':
            Crafty(msg.entityId).setBlockType(this.placeBlockType);
            break;
          default:
            break;
        }
      });

      var self = this;
      this.bind('KeyDown', function keyDown(e) {
        switch (this.clickMode) {
          case 'unitCommand':
            if (e.key == Crafty.keys.SHIFT) {
              this.selectMode = 'add';
            }
            break;
          case 'blockChange':
            if (e.key == Crafty.keys['1']) {
              this.placeBlockType = 'frontRightUp';
            }
            if (e.key == Crafty.keys['2']) {

              let width = 300,
                height = 300,
                buffer = new Uint8ClampedArray(width * height * 4);

              for (let y = 0; y < height; y++) {
                for (let x = 0; x < width; x++) {
                  let pos = (y * width + x) * 4; // position in buffer based on x and y
                  buffer[pos] = 200;           // some R value [0, 255]
                  buffer[pos + 1] = 200;           // some G value
                  buffer[pos + 2] = 200;           // some B value
                  buffer[pos + 3] = 255;           // set alpha channel
                }
              }

              let imageData = new ImageData(buffer, width, height);
              Crafty.asset('test2.png', imageData);
              Crafty.sprite('test2.png', {flower2: [0, 0, width, height]});
              //let flower_entity = Crafty.e('2D, WebGL, flower2');

              let unit = Crafty('Unit');
              unit.removeComponent('bot_RightDown');
              unit.addComponent('flower2');

            }
            if (e.key == Crafty.keys['3']) {
              let canvas = document.createElement('canvas'),
                context = canvas.getContext('2d');

              let img = Crafty.asset('img/sprites/bot.png');
              canvas.width = img.width;
              canvas.height = img.height;
              context.drawImage(img, 0, 0);

              let myData = context.getImageData(0, 0, img.width, img.height),
                pixelData = myData.data,
                localImage = new ImageData(pixelData, myData.width, myData.height);

              for (let y = 0; y < myData.height; y++) {
                for (let x = 0; x < myData.width; x++) {
                  let pos = (y * myData.width + x) * 4; // position in buffer based on x and y
                  /*                    //myData.data[pos  ] += 50;           // some R value [0, 255]
                   myData.data[pos + 2] = 200;           // some B value
                   myData.data[pos + 3] = 200;           // some B value*/
                }
              }

              Crafty.asset('test.png', localImage);
              //Crafty.sprite('test.png', {flower: [0, 0, myData.width, myData.height]});
              Crafty.sprite(localImage.width, localImage.height, 'test.png', {flower: [0, 0]});
              let flower_entity = Crafty.e('2D, WebGL, flower');
              //console.log(flower_entity);
            }
            if (e.key == Crafty.keys['4']) {
              let canvas = document.createElement('canvas'),
                context = canvas.getContext('2d');
/*              var rectangle = new Path2D();
              rectangle.rect(10, 10, 50, 50);

              var circle = new Path2D();
              circle.moveTo(125, 35);
              circle.arc(100, 35, 25, 0, 2 * Math.PI);

              context.stroke(rectangle);
              context.fill(circle);

              let myData = context.getImageData(0, 0, img.width, img.height),
                pixelData = myData.data,
                localImage = new ImageData(pixelData, myData.width, myData.height);

              Crafty.asset('test.png', localImage);
              Crafty.sprite(localImage.width, localImage.height, 'test.png', {flower: [0, 0]});
              let flower_entity = Crafty.e('2D, WebGL, flower');*/
              
/*              unit.removeComponent('bot_RightDown');
              unit.addComponent('');*/
              //unit.trigger('Invalidate')
            }
            break;
        }
      });
      this.bind('KeyUp', function keyUp(e) {
        if (e.key == Crafty.keys.SHIFT) {
          this.selectMode = 'single';
        }
      });
      this.bind('MouseWheelScroll', function mouseWheelScroll(e) {
        //console.log(e);
        //console.log(Crafty.lastEvent);
        //console.log(Crafty.viewport);
        if (e.direction === 1) {
          //Crafty.viewport.zoom(1.1, Crafty.lastEvent.realX, Crafty.lastEvent.realY, 100);
          Crafty.viewport.zoom(1.1, Crafty.viewport._x, Crafty.viewport._y, 100);
          //Crafty.viewport.w/2
          //Crafty.viewport.scroll('_x', 500);
        } else {
          //Crafty.viewport.zoom(0.9, Crafty.lastEvent.realX, Crafty.lastEvent.realY, 100);
          Crafty.viewport.zoom(0.9, Crafty.viewport._x, Crafty.viewport._y, 100);
        }
        //Crafty.viewport.scale(Crafty.viewport._scale * (1 + evt.direction * 0.1));
        //Crafty.viewport.zoom(2, 100, 100, 3000);
      });

      this.bind('EnterFrame', function enterFrame(msg) {
        if (this.viewportMoveX != 0) {
          Crafty.viewport.x += this.viewportMoveX;
          //console.log(Crafty('WebGL').toArray().length);
        }
        if (this.viewportMoveY != 0) {
          Crafty.viewport.y += this.viewportMoveY;
          //console.log(Crafty('WebGL').toArray().length);
        }
        //Crafty('Panel').drawPanel();
      });

      Crafty.addEvent(Crafty, Crafty.stage.elem, 'mousemove', function (e) {
        //console.log(e, Crafty.viewport);
        //console.log(e);
        if ((e.clientX === 0) || (e.clientX === Crafty.viewport._width - 1)) {
          if (e.clientX === 0) {
            self.viewportMoveX = 10;
          }
          if (e.clientX === Crafty.viewport._width - 1) {
            self.viewportMoveX = -10;
          }
        } else {
          self.viewportMoveX = 0;
        }

        if ((e.clientY === 0) || (e.clientY === Crafty.viewport._height - 1)) {
          if (e.clientY === 0) {
            self.viewportMoveY = 10;
          }
          if (e.clientY === Crafty.viewport._height - 1) {
            self.viewportMoveY = -10;
          }
        } else {
          self.viewportMoveY = 0;
        }

      });

      Crafty.addEvent(Crafty, Crafty.stage.elem, 'mousedown', function (e) {

        function rect(e) {
          Crafty('SelectRect').drawSelectRect({
            x: e.realX,
            y: e.realY
          });

          self.selectedUnits.forEach(function (element) {
            Crafty(element).trigger('GameInterface:Deselect');
          });
          self.selectedUnits = [];

        }

        /*        function scroll(e) {
         var dx = base.x - e.clientX,
         dy = base.y - e.clientY;

         base = {
         x: e.clientX, y: e.clientY
         };

         Crafty.viewport.x -= dx;
         Crafty.viewport.y -= dy;

         //Crafty('Panel').drawPanel();
         }*/
        switch (e.mouseButton) {
          case 0:
            /*            selectRect.createSelectRect({
             x: e.realX,
             y: e.realY
             });
             Crafty.addEvent(this, Crafty.stage.elem, 'mousemove', rect);
             Crafty.addEvent(this, Crafty.stage.elem, 'mouseup', function () {
             Crafty('SelectRect').destroy();
             Crafty.removeEvent(this, Crafty.stage.elem, 'mousemove', rect);
             });*/
            break;
          case 1:

            break;
          case 2:
            /*            if (self.selectedUnits.length > 0) {
             self.selectedUnits.forEach(function (element) {
             Crafty(element).trigger('GameInterface:Deselect');
             });
             self.selectedUnits = [];
             }
             var base = {
             x: e.clientX,
             y: e.clientY
             };
             Crafty.addEvent(this, Crafty.stage.elem, 'mousemove', scroll);
             Crafty.addEvent(this, Crafty.stage.elem, 'mouseup', function () {
             Crafty.removeEvent(this, Crafty.stage.elem, 'mousemove', scroll);
             });*/
            break;
          default:

        }
      });

    }
  });

};

var createInterface = function createInterface() {
  'use strict';

  Crafty.e('GameInterface');
  //.trigger('GameInterface:DrawPanel');

  /*  Crafty.e('GameInterface')
   .attr({
   x: 100,
   y: 300,
   w: 50,
   h: 7,
   z: 10000
   })
   .drawLine({x:100, y:300}, {x:150, y:350});*/

  let menuButton = Crafty.e('Button')
    .setCallbacks(
      () => {
        console.log('clicked');
      }, () => {
        menuButton.color('green');
      }, () => {
        menuButton.color('#FF6F59');
      })
    .attr({
      x: 200,
      y: 200,
      w: 200,
      h: 100
    })
    .color('#FF6F59');

};

createComponent();
module.exports.createInterface = createInterface;
