/**
 * Created by Redfox on 25.09.2015.
 */

const Crafty = require('craftyjs');  

var createComponent = function createComponent() {
  'use strict';

  Crafty.c('SelectRect', {
    init: function init() {
      this.requires('2D, WebGL, Color');

      this._startPoint = {};
      this._endPoint = {};
      this._leftTopPoint = {};
      this._rightDownPoint = {};
      
      this.bind('Draw', this._drawMe);
    },
    _drawMe: function _drawMe(e) {

      var ctx = e.ctx;
      ctx.lineWidth = 1;
      ctx.strokeStyle = 'white';

      ctx.strokeRect(this._leftTopPoint.x + 0.5, this._leftTopPoint.y + 0.5, this.w - 2, this.h - 2);
      

      return this;
    },
    drawSelectRect: function drawSelectRect(endPoint) {
/*      this._startPoint.x = this._startPoint.x + 32;
      this._startPoint.y = this._startPoint.x + 64;*/

      if(endPoint.x < Crafty.viewport.width - 200) {
        this._endPoint.x = endPoint.x;
      } else {
        this._endPoint.x = Crafty.viewport.width - 200;
      }
      this._leftTopPoint.x = Math.min(this._startPoint.x, this._endPoint.x);
      this._rightDownPoint.x = Math.max(this._startPoint.x, this._endPoint.x);
      this.x = this._leftTopPoint.x - 1;
      this.w = Math.abs(this._startPoint.x - this._endPoint.x) + 1;

      this.ready = true;
      
      if(endPoint.y < Crafty.viewport.height) {
        this._endPoint.y = endPoint.y;
        this._leftTopPoint.y = Math.min(this._startPoint.y, this._endPoint.y);
        this._rightDownPoint.y = Math.max(this._startPoint.y, this._endPoint.y);
        this.y =this._leftTopPoint.y - 1;
        this.h = Math.abs(this._startPoint.y - this._endPoint.y) + 1;
        this.ready = true;
      }

      //console.log(this._leftTopPoint, this._rightDownPoint);

      return this;
    }
  });

};

var createSelectRect = function createSelectRect(msg) {
  'use strict';
  return Crafty.e('SelectRect')
    .addComponent('SelectRect')
    //.color('white')
    .attr({
      z: 500000,
      _startPoint: {
        x: msg.x,
        y: msg.y
      }
    })
};

createComponent();
module.exports.createSelectRect = createSelectRect;
