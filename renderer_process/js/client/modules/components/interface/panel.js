/**
 * Created by Redfox on 19.09.2015.
 */

const Crafty = require('craftyjs');  
  
var createComponent = function createComponent() {
  'use strict';

  Crafty.c('Panel', {
    init: function init() {
      this.requires('2D, WebGL, Mouse, Color');

      this.ready = false;

      this.bind('Draw', this._drawMe);
      this.bind('MouseDown', function mouseDown(e) {
        if (e.button < 2) {
          
        }
      });
    },
    _drawMe: function _drawMe(e) {
      
/*      this.x = 1280 - 200;
      this.y = 0;*/
      
      //this.ctx = e.ctx;
      //e.ctx.lineWidth = 1;
      //e.ctx.strokeStyle = '#00A800';
      //e.ctx.fillStyle = '#00A800';
      
      //e.ctx.fillRect(Crafty.viewport._width - 200, 0, 200, Crafty.viewport._height);
      //e.ctx.fillRect(this._endPoint.x - 2, this._endPoint.y - 2, 4, 4);

/*      e.ctx.beginPath();
      e.ctx.moveTo(this._startPoint.x, this._startPoint.y);
      e.ctx.lineTo(this._endPoint.x, this._endPoint.y);
      e.ctx.stroke();
      e.ctx.closePath();*/

      return this;
    },
    drawPanel: function drawPanel() {

/*      this.x = Math.min(this._startPoint.x, this._endPoint.x) - 2;
      this.y = Math.min(this._startPoint.y, this._endPoint.y) - 2;
      this.w = Math.abs(this._startPoint.x - this._endPoint.x) + 4;
      this.h = Math.abs(this._startPoint.y - this._endPoint.y) + 4;*/
      this.x = -Crafty.viewport.x + Crafty.viewport.width - this.w;
      this.y = -Crafty.viewport.y;
      
      //console.log(this);
      
      //this.ready = true;

      return this;
    }
  });

};
var createPanel = function createPanel() {
  'use strict';

  Crafty.e('Panel')
    .addComponent('Panel')
    .color('red')
    .attr({
      w: 200,
      h: Crafty.viewport._height,
      z: 500000,
      ready: true
    })
    .drawPanel();

};

createComponent();
module.exports.createPanel = createPanel;
