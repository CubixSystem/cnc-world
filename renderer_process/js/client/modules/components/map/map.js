/**
 * Created by Redfox on 03.09.2015.
 */

const Crafty = require('craftyjs');

const groundBlock = require('./../ground-block/ground-block');
const noise = require('./../quick-noise');
//var noise = require('quick-noise-js');

var createComponent = function createComponent() {
  'use strict';

  Crafty.c('MapModule', {
    init: function init() {

    },
    generateMap: function generateMap(iso, map) {
      let msg = {};
      let resolution = 30;
      let temp;
      let thing;
      
      for (let y = 0; y < map.size.y; y++) {
        for (let x = 0; x < map.size.x; x++) {
          msg = {
            iso: iso,
            type: 'flat',
            x: x,
            y: y
          };
          
          //msg.z = Crafty.math.randomInt(0, 1)/2;
          
/*          temp = noise.noise(x/resolution, y/resolution, 0);
          temp = temp.toFixed(1) * 5;*/
          
          //console.log(temp, temp/2);
          temp = 0;
/*          if(temp <= 0.5 ) {
            temp = 0.5;
          } else {
            temp = 1;
          }*/
          //msg.z = Math.round(temp).toFixed(1);
          //msg.z = Crafty.math.randomInt(0, 4) / 2;
          msg.z = temp;
          
          groundBlock.createGroundBlock(msg);

          switch (Crafty.math.randomInt(12, 12)) {
            case 3:
              thing = Crafty.e('2D, WebGL');
              thing.addComponent('grass_1');
              //console.log(thing);
              iso.place(thing, msg.x, msg.y, msg.z + 0.5);
              //this.addComponent('grass_1');
              break;
            case 4:
              thing = Crafty.e('2D, WebGL');
              thing.addComponent('flower_1');
              iso.place(thing, msg.x, msg.y, msg.z + 0.5);
              //this.addComponent('flower_1');
              break;
            case 5:
              thing = Crafty.e('2D, WebGL');
              thing.addComponent('grass_2');
              iso.place(thing, msg.x, msg.y, msg.z + 0.5);
              break;
            case 6:
            case 7:
              thing = Crafty.e('2D, WebGL');
              thing.addComponent('grass_3');
              iso.place(thing, msg.x, msg.y, msg.z + 0.5);
              break;
            case 8:
              thing = Crafty.e('2D, WebGL');
              thing.addComponent('flower_2');
              iso.place(thing, msg.x, msg.y, msg.z + 0.5);
              break;
            case 9:
              thing = Crafty.e('2D, WebGL');
              thing.addComponent('grass_4');
              iso.place(thing, msg.x, msg.y, msg.z + 0.5);
              break;
            default:
              //this.addComponent('empty');
              break;
          }
          
              //Crafty(msg.entityId).setBlockType(this.placeBlockType);
          
            //console.log(temp);
/*            for (let s = -2; s < temp; s += 0.5) {
              msg = {
                iso: iso,
                type: 'flat',
                x: x,
                y: y
              };
              msg.z = s;
              groundBlock.createGroundBlock(msg);
            }*/
          
          //console.log(Crafty('WebGL').toArray().length);
        }
      }

      let currentBlock;
      let nextBlock;
      let block0;
      let t3;
      for (let y = 0; y < map.size.y; y++) {
        for (let x = 0; x < map.size.x; x++) {
          currentBlock = Crafty(x.toString() + '.' + y.toString());

          block0 = Crafty((x).toString() + '.' + (y).toString());
          //console.log(block0);
          
/*          t3 = x+1;
          nextBlock = Crafty(t3.toString() + '.' + y.toString());
          //console.log(nextBlock, nextBlock.isoCoord);
          if(nextBlock.isoCoord) {
            if (nextBlock.isoCoord.z < currentBlock.isoCoord.z) {
              currentBlock.setBlockType('frontRightUp');
            }
          }

          t3 = x-1;
          nextBlock = Crafty(t3.toString() + '.' + y.toString());
          if(nextBlock.isoCoord) {
            if (nextBlock.isoCoord.z < currentBlock.isoCoord.z) {
              currentBlock.setBlockType('backLeftUp');
            }
          }

          t3 = y+1;
          nextBlock = Crafty(x.toString() + '.' + t3.toString());
          if(nextBlock.isoCoord) {
            if (nextBlock.isoCoord.z < currentBlock.isoCoord.z) {
              currentBlock.setBlockType('frontLeftUp');
            }
          }

          t3 = y-1;
          nextBlock = Crafty(x.toString() + '.' + t3.toString());
          if(nextBlock.isoCoord) {
            if (nextBlock.isoCoord.z < currentBlock.isoCoord.z) {
              currentBlock.setBlockType('backRightUp');
            }
          }*/
          
        }
      }

/*      msg = {
        iso: iso,
        type: 'flat',
        x: 7,
        y: 7,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'frontUp',
        x: 8,
        y: 8,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'leftUp',
        x: 6,
        y: 8,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'rightUp',
        x: 8,
        y: 6,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'backUp',
        x: 6,
        y: 6,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'frontLeftUp',
        x: 7,
        y: 8,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'backLeftUp',
        x: 6,
        y: 7,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'frontRightUp',
        x: 8,
        y: 7,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);

      msg = {
        iso: iso,
        type: 'backRightUp',
        x: 7,
        y: 6,
        z: 0.5
      };
      groundBlock.createGroundBlock(msg);*/
      
      
      return this;
    }
  });
};
var createMap = function createMap(iso, map) {
  'use strict';
  let mapModule = Crafty.e('MapModule')
    .generateMap(iso, map);
};

createComponent();
module.exports.createMap = createMap;
