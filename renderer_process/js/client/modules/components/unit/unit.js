const Crafty = require('craftyjs');

const createComponent = function createComponent() {
  'use strict';
  const onEnterFrame = require('./enterFrame');
  Crafty.c('Unit', {
    _eventsToListen: {
      'Crafty:MouseDown': 'MouseDown',
      'Crafty:EnterFrame': 'EnterFrame',
      'PathfindModule:PathReady': 'PathfindModule:PathReady',
      'PathfindModule:PathNotFound': 'PathfindModule:PathNotFound',
      'PathfindModule:GetBlockWalkableResponse': 'PathfindModule:GetBlockWalkableResponse',
      'GameInterface:SelectedByPlayer': 'GameInterface:SelectedByPlayer',
      'GameInterface:Deselect': 'GameInterface:Deselect',
      'GameInterface:MoveToPoint': 'GameInterface:MoveToPoint'
    },
    _eventsToEmit: {
      'PathfindModule:FindPath': 'PathfindModule:FindPath',
      'PathfindModule:MoveUnit': 'PathfindModule:MoveUnit',
      'PathfindModule:GetBlockWalkable': 'PathfindModule:GetBlockWalkable',
      'GameInterface:DrawPathLine': 'GameInterface:DrawPathLine',
      'GameInterface:UpdatePathLine': 'GameInterface:UpdatePathLine',
      'GameInterface:DestroyPathLine': 'GameInterface:DestroyPathLine',
      'GameInterface:UnitClicked': 'GameInterface:UnitClicked'
    },
    init: function init() {
      this.addComponent('2D, WebGL, Sprite, Mouse, Color');
      this.addComponent('bot_RightDown');

      this.step = {};
      this.isoDelta = {x: 0, y: 0};
      this.direction = '';
      this.nextPointId = '';
      this.isSelected = false;

      this.haveOrder = false;
      this.havePathLine = false;
      this.isSpriteReachedPoint = true;

      this.refuseCounter = 0;
      this.isGetBlockWalkablePending = false;

      this.bind(this._eventsToListen['Crafty:MouseDown'], function mouseDown(e) {
        if (e.button < 2) {
          //this.destroy();
          //console.log(e);
          //console.log(this.iso);
          /*          console.log(e.clientX, e.clientY);
           var px = this.iso.px2pos(e.clientX, e.clientY);
           console.log(px); //Object { x=100, y=100}*/
          var msg = {
            id: this.getId()
          };
          Crafty.trigger(this._eventsToEmit['GameInterface:UnitClicked'], msg);
        }
        //}).areaMap([32, 32 + 32 - 16, 0, 48 + 32 - 16, 32, 64 + 32 - 16, 64, 48 + 32 - 16]); //not so elegant...
      }).areaMap([48, 78, 0, 103, 48, 128, 96, 103]);
      this.bind(this._eventsToListen['Crafty:EnterFrame'], function enterFrame(eventData) {
        onEnterFrame.onEnterFrame(eventData, this);
      });

      this.bind(this._eventsToListen['PathfindModule:PathReady'], function pathReady(msg) {
        this.path = msg.path;
        this.stepNumber = msg.stepNumber;
        this.nextPoint = msg.nextPoint;
        this.moveTarget = this.iso.pos2px(this.nextPoint.x, this.nextPoint.y);
        this.isSpriteReachedPoint = false;

        this.isoDelta.x = this.isoCoord.x - this.nextPoint.x;
        this.isoDelta.y = this.isoCoord.y - this.nextPoint.y;

        /*        function destroyPathLine() {
         Crafty.trigger(this._eventsToEmit['GameInterface:DestroyPathLine'], this.getId());
         this.havePathLine = false;
         }
         if(this.havePathLine === false) {
         Crafty.trigger(this._eventsToEmit['GameInterface:DrawPathLine'], {
         id: this.getId(),
         startPoint: {left: this.x, top: this.y},
         endPoint: this.iso.pos2px(this.target.isoCoord.x, this.target.isoCoord.y)
         });
         this.havePathLine = true;
         setTimeout(destroyPathLine.bind(this), 1000);
         }*/

        //console.log(this.path);
        /*        if(this.stepNumber < this.path.length -1) {

         }*/

        //console.log(msg);
        this.startMoving();
      });
      this.bind(this._eventsToListen['PathfindModule:PathNotFound'], function pathNotFound() {

        console.log('Path was not found.');
        console.log(this.isoCoord);
        console.log(this.path);
        
        this.isMoving = false;
        if (this.havePathLine === true) {
          Crafty.trigger(this._eventsToEmit['GameInterface:DestroyPathLine'], this.getId());
          this.havePathLine = false;
        }
      });
      this.bind(this._eventsToListen['PathfindModule:GetBlockWalkableResponse'], function getBlockWalkableResponse(msg) {
        this.isGetBlockWalkablePending = false;
        if (msg.value === 0) {
          //console.log('Next point clear!');

          this.stepNumber = this.stepNumber + 1;
          this.nextPoint = this.path[this.stepNumber];
          this.moveTarget = this.iso.pos2px(this.nextPoint.x, this.nextPoint.y);
          this.refuseCounter = 0;

          onEnterFrame.moveIsoBase(this);
          /*          Crafty.trigger(this._eventsToEmit['PathfindModule:SetBlockWalkable'], {
           value: 1,
           isoCoord: {
           x: this.nextPoint.x,
           y: this.nextPoint.y
           }
           });*/

        } else {
          //console.log('Next point NOT clear!');

          Crafty.trigger(this._eventsToEmit['PathfindModule:FindPath'], {
            ownerInGameId: this.getId(),
            startPointIsoCoord: {
              x: this.isoCoord.x,
              y: this.isoCoord.y
            },
            endPointIsoCoord: {
              x: this.target.isoCoord.x,
              y: this.target.isoCoord.y
            }
          });

          this.refuseCounter++;
          if (this.refuseCounter === 25) {
            console.log('Path dropped!');
            this.stepNumber = this.path.length - 1;
          }
        }
      });

      this.bind(this._eventsToListen['GameInterface:MoveToPoint'], function moveToPoint(msg) {
        function destroyPathLine() {
          if (this.havePathLine === true) {
            Crafty.trigger(this._eventsToEmit['GameInterface:DestroyPathLine'], this.getId());
            this.havePathLine = false;
          }
        }

        if (this.isSelected === true) {
          this.moveToPoint(msg);

          if (this.havePathLine === true) {
            Crafty.trigger(this._eventsToEmit['GameInterface:DestroyPathLine'], this.getId());
            this.havePathLine = false;
          }

          Crafty.trigger(this._eventsToEmit['GameInterface:DrawPathLine'], {
            id: this.getId(),
            startPoint: {left: this.x, top: this.y},
            endPoint: this.iso.pos2px(this.target.isoCoord.x, this.target.isoCoord.y)
          });
          this.havePathLine = true;
          setTimeout(destroyPathLine.bind(this), 500);
        }
      });
      this.bind(this._eventsToListen['GameInterface:SelectedByPlayer'], function selectedByPlayer() {
        this.isSelected = true;
        this.trigger('Invalidate');
        //console.log('this.isSelected',  this.isSelected);

        function destroyPathLine() {
          if (this.havePathLine === true) {
            Crafty.trigger(this._eventsToEmit['GameInterface:DestroyPathLine'], this.getId());
            this.havePathLine = false;
          }
        }

        if ((this.isMoving === true) && (this.havePathLine === false)) {
          Crafty.trigger(this._eventsToEmit['GameInterface:DrawPathLine'], {
            id: this.getId(),
            startPoint: {left: this.x, top: this.y},
            endPoint: this.iso.pos2px(this.target.isoCoord.x, this.target.isoCoord.y)
          });
          this.havePathLine = true;
          setTimeout(destroyPathLine.bind(this), 500);
        }

      });
      this.bind(this._eventsToListen['GameInterface:Deselect'], function deselect() {
        this.isSelected = false;
        this.trigger('Invalidate');
        console.log('Unit deselected!');
        //console.log('this.isSelected',  this.isSelected);
      });

      this.bind('Draw', this._drawMe);
    },
    _drawMe: function _drawMe(e) {
      //console.log(e);

/*      //this.ctx = e.ctx;
      e.ctx.lineWidth = 1;
      e.ctx.strokeStyle = '#00A800';
      e.ctx.fillStyle = 'red';

      var ttt = this.iso.pos2px(this.isoCoord.x, this.isoCoord.y);
      e.ctx.fillRect(ttt.left + 32 - 2, ttt.top - 32 - 1, 2, 3);

      e.ctx.fillStyle = 'yellow';
      if (this.nextPoint) {
        ttt = this.iso.pos2px(this.nextPoint.x, this.nextPoint.y);
        e.ctx.fillRect(ttt.left + 32, ttt.top - 32 - 1, 2, 3);
      }

      if (this.isSelected) {
        //this.color("rgba(0, 255, 0, 0.5)");
        //this.assignColor('#550000');
        /!*        console.log(this);
         console.log(e);*!/

        /!*        var ff = this.iso.pos2px(this.isoCoord.x, this.isoCoord.y);
         var imageData =  e.ctx.getImageData(ff.left, ff.top, this.w, this.h);
         //console.log(imageData);
         for (var i = 0; i < imageData.data.length; i += 4) {
         var r = imageData.data[i];
         var g = imageData.data[i + 1];
         var b = imageData.data[i + 2];
         var a = imageData.data[i + 3];

         if((r!=0)&&(g!=0)&&(b!=0)&&(a!=0)) {
         imageData.data[i] = 255;
         }

         imageData.data[i] = 255;
         //imageData.data[i + 3] = 255;
         }
         e.ctx.putImageData(imageData, this.x, this.y);
         //console.log(imageData);*!/

        e.ctx.strokeStyle = 'white';
        e.ctx.beginPath();
        e.ctx.moveTo(this.x + 32 + 10, this.y + 64 + 16);
        e.ctx.lineTo(this.x + 32 + 10 + 8 - 0.5, this.y + 64 + 16 - 0.5);
        e.ctx.lineTo(this.x + 32 + 10 + 8 - 0.5, this.y + 64 + 16 - 8 - 0.5);

        e.ctx.moveTo(this.x + 32 - 10, this.y + 64 + 16);
        e.ctx.lineTo(this.x + 32 - 10 - 8 - 0.5, this.y + 64 + 16 - 0.5);
        e.ctx.lineTo(this.x + 32 - 10 - 8 - 0.5, this.y + 64 + 16 - 8 - 0.5);

        e.ctx.moveTo(this.x + 32 + 10, this.y + 64 - 16);
        e.ctx.lineTo(this.x + 32 + 10 + 8, this.y + 64 - 16);
        e.ctx.lineTo(this.x + 32 + 10 + 8, this.y + 64 - 16 + 8);

        e.ctx.moveTo(this.x + 32 - 10, this.y + 64 - 16);
        e.ctx.lineTo(this.x + 32 - 10 - 8 - 0.5, this.y + 64 - 16 - 0.5);
        e.ctx.lineTo(this.x + 32 - 10 - 8 - 0.5, this.y + 64 - 16 + 8 - 0.5);

        e.ctx.stroke();
        e.ctx.closePath();
      }*/

      /*
       e.ctx.beginPath();
       e.ctx.moveTo(this._startPoint.x, this._startPoint.y);
       e.ctx.lineTo(this._endPoint.x, this._endPoint.y);
       e.ctx.stroke();
       e.ctx.closePath();
       */

      return this;
    },
    startMoving: function startMoving() {
      //console.log('Start Moving!');
      this.isMoving = true;
      this.haveOrder = true;

      this.getDirection();

      return this;
    },
    getDirection: function calculateDirection() {
      var isoDeltaX = this.isoDelta.x,
      isoDeltaY = this.isoDelta.y;
      
      //console.log(this.isoDelta);
      
      if ((isoDeltaX === 0) && (isoDeltaY === 1)) {
        this.direction = 'NE';
        this.setDirection('TopRight');
      } else {
        if ((isoDeltaX === -1) && (isoDeltaY === 1)) {
          this.direction = 'E';
          this.setDirection('Right');
        } else {
          if ((isoDeltaX === -1) && (isoDeltaY === 0)) {
            this.direction = 'SE';
            this.setDirection('DownRight');
          } else {
            if ((isoDeltaX === -1) && (isoDeltaY === -1)) {
              this.direction = 'S';
              this.setDirection('Down');
            } else {
              if ((isoDeltaX === 0) && (isoDeltaY === -1)) {
                this.direction = 'SW';
                this.setDirection('DownLeft');
              } else {
                if ((isoDeltaX === 1) && (isoDeltaY === -1)) {
                  this.direction = 'W';
                  this.setDirection('Left');
                } else {
                  if ((isoDeltaX === 1) && (isoDeltaY === 0)) {
                    this.direction = 'NW';
                    this.setDirection('TopLeft');
                  } else {
                    this.direction = 'N';
                    this.setDirection('Top');
                  }
                }
              }
            }
          }
        }
      }
      
      return this;
    },
    setDirection: function setDirection(direction) {
      switch (direction) {
        case 'Top':
          this.sprite(2, 0);
          break;
        case 'Down':
          this.sprite(1, 1);
          break;
        case 'Left':
          this.sprite(0, 0);
          break;
        case 'Right':
          this.sprite(4, 0);
          break;
        case 'TopRight':
          this.sprite(3, 0);
          break;
        case 'DownRight':
          this.sprite(0, 1);
          break;
        case 'TopLeft':
          this.sprite(1, 0);
          break;
        case 'DownLeft':
          this.sprite(2, 1);
          break;
      }
      return this;
    },
    isoPlace: function isoPlace(x, y, z, iso) {
      this.iso = iso;

      this.isoCoord = {
        x: x,
        y: y,
        z: z
      };

      this.iso.place(this, this.isoCoord.x, this.isoCoord.y, this.isoCoord.z);
      this.y += 24;

      return this;
    },
    moveToPoint: function moveToPoint(target) {
      
      this.target = {};
      this.target.isoCoord = Crafty(target.entityId).isoCoord;
      
      //console.log(this.isoCoord, this.target.isoCoord);
      Crafty.trigger(this._eventsToEmit['PathfindModule:FindPath'], {
        ownerInGameId: this.getId(),
        startPointIsoCoord: {
          x: this.isoCoord.x,
          y: this.isoCoord.y
        },
        endPointIsoCoord: {
          x:  this.target.isoCoord.x,
          y:  this.target.isoCoord.y
        }
      });

      return this;
    }
  });

};
var createUnit = function createUnit(iso, coords) {
  'use strict';

  var unit = Crafty.e('Unit')
    .setDirection('Left')
    .isoPlace(coords.x, coords.y, coords.z, iso);
};

createComponent();
module.exports.createUnit = createUnit;
