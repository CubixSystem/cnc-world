/**
 * Created by Redfox on 22.09.2015.
 */

const Crafty = require('craftyjs');

var updatePathLine = function updatePathLine(unit) {
  'use strict';
  Crafty.trigger(unit._eventsToEmit['GameInterface:UpdatePathLine'], {
    unitInGameId: unit.inGameId,
    startPoint: {left: unit.x, top: unit.y},
    endPoint: unit.iso.pos2px(unit.target.isoCoord.x, unit.target.isoCoord.y)
  });
};

var calculateDistance = function calculateDistance(unit) {
  'use strict';
  unit.dx = Math.round(unit.moveTarget.x - unit.x);
  unit.dy = Math.round(unit.moveTarget.y - unit.y - 24); //24?
  //unit.dy = Math.round(unit.moveTarget.y - unit.y); //24?
};

var calculateStepSize = function calculateStepSize(eventData, unit) {
  unit.step.xstep = 64 * (eventData.dt / 1000);
  unit.step.ystep = unit.step.xstep;

  if ((unit.direction === 'NE') || (unit.direction === 'SE') ||
    (unit.direction === 'NW') || (unit.direction === 'SW')) {
    unit.step.ystep = unit.step.ystep / 2;
  }
  //console.log(this.step);
};

var moveSprite = function moveSprite(unit) {
  'use strict';
  if ((unit.dx != 0) || (unit.dy != 0)) {
    unit.isSpriteReachedPoint = false;
    if (Crafty.math.abs(unit.dx) < unit.step.xstep) {
      unit.x = unit.moveTarget.x;
    } else {
      if (unit.dx > 0) {
        unit.x += unit.step.xstep;
      } else {
        unit.x -= unit.step.xstep;
      }
    }

    if (Crafty.math.abs(unit.dy) < unit.step.ystep) {
      unit.y = unit.moveTarget.y - 24;  //24?
      //unit.y = unit.moveTarget.y;  //24?
    } else {
      if (unit.dy > 0) {
        unit.y += unit.step.ystep;
      } else {
        unit.y -= unit.step.ystep;
      }
    }

  } else {
    unit.isSpriteReachedPoint = true;
  }
};

module.exports.updatePathLine = updatePathLine;
module.exports.calculateDistance = calculateDistance;
module.exports.calculateStepSize = calculateStepSize;
module.exports.moveSprite = moveSprite;
