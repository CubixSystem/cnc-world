/**
 * Created by Redfox on 21.09.2015.
 */

const Crafty = require('craftyjs');

var graphicsMetods = require('./graphicsMetods');

var moveIsoBase = function moveIsoBase(unit) {
  'use strict';
  Crafty.trigger(unit._eventsToEmit['PathfindModule:MoveUnit'], {
    oldIsoCoord: {
      x: unit.isoCoord.x,
      y: unit.isoCoord.y
    },
    newIsoCoord: {
      x: unit.nextPoint.x,
      y: unit.nextPoint.y
    }
  });

  unit.isoDelta.x = unit.isoCoord.x - unit.nextPoint.x;
  unit.isoDelta.y = unit.isoCoord.y - unit.nextPoint.y;
  
  unit.isoCoord.x = unit.nextPoint.x;
  unit.isoCoord.y = unit.nextPoint.y;

  unit.getDirection();
};


var checkIsMoving = function checkIsMoving(eventData, unit) {
  'use strict';
  if (unit.isMoving === true) {
    if (unit.havePathLine === true) {
      graphicsMetods.updatePathLine(unit);
    }
    graphicsMetods.calculateDistance(unit);
    graphicsMetods.calculateStepSize(eventData, unit);
    graphicsMetods.moveSprite(unit);
    
    unit.nextPointId = unit.nextPoint.x.toString() + '.' + unit.nextPoint.y.toString();
    unit.currentPointId = unit.isoCoord.x.toString() + '.' + unit.isoCoord.y.toString();
    //unit.z = Crafty(unit.nextPointId).z + 1000;
    //unit.z = Crafty(unit.nextPointId).z + 1;
    //console.log(unit.z);
    //unit.z = Crafty(unit.currentPointId).z + 1;
    unit.z = Crafty(unit.currentPointId).z + 1000;
    //unit.isoCoord.z = Crafty(unit.currentPointId).isoCoord.z + 0.5;
    //unit.z = unit.getZAtLoc(unit.isoCoord.x, unit.isoCoord.y, unit.isoCoord.z + 0.5);
    
    if(unit.isSpriteReachedPoint === true) {
      if (unit.stepNumber < unit.path.length - 1) {
        if(unit.isGetBlockWalkablePending === false) {
          unit.isGetBlockWalkablePending = true;
          Crafty.trigger(unit._eventsToEmit['PathfindModule:GetBlockWalkable'], {
            ownerInGameId: unit.getId(),
            isoCoord: unit.path[unit.stepNumber + 1]
          });
        }
      } else {
        unit.isMoving = false;
        if (unit.havePathLine === true) {
          Crafty.trigger(unit._eventsToEmit['GameInterface:DestroyPathLine'], unit.getId());
          unit.havePathLine = false;
        }
        unit.haveOrder = false;
      }
      //console.log('~~END~~');
    }
  }
};
  
var onEnterFrame = function onEnterFrame(eventData, context) {
  checkIsMoving(eventData, context);
};

module.exports.onEnterFrame = onEnterFrame;
module.exports.moveIsoBase = moveIsoBase;
