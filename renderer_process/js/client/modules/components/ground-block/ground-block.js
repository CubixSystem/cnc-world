/**
 * Created by Redfox on 16.08.2015.
 */

const Crafty = require('craftyjs');

const createComponent = function createComponent(blocksConfig) {
  'use strict';
  Crafty.c('GroundBlock', {
    init: function init() {

      this.addComponent('2D, Mouse, Sprite');

      this.addComponent('WebGL');

      this.blocksConfig = blocksConfig;

      this.bind('MouseDown', function mouseDown(e) {
        switch (e.mouseButton) {
          case 0:
            Crafty.trigger('GroundBlock:ClickedHere', {
              entityId: this.getId()
            });
            break;
          /*
           if (this.has('WebGL') === false) {
           this.addComponent('WebGL');
           } else {
           if (this.has('WebGL') === true) {
           this.removeComponent('WebGL', true);
           }
           }
           */

        }
      }).areaMap([48, 26, 0, 49, 48, 71, 96, 49]);
      this.bind('ShowPath', function showPath(msg) {
        console.log('showPath!!');
        /*        var self = this;
         msg.path.some(function (element) {
         if ((element.x === self.isoCoord.x) && ((element.y === self.isoCoord.y))) {
         //e.sprite(2, 0);
         //e.iso.place(Crafty.e('2D, Canvas, select'), e.isoCoord.x, e.isoCoord.y, e.isoCoord.z + 1);
         return true;
         }
         });*/

      });
      /*      this.bind('EnterFrame', function enterFrame(msg) {
       //if ((this._x >= -Crafty.viewport._x) && (this._x <= -Crafty.viewport._x + Crafty.viewport._width)) {
       /!*        if (Crafty.DrawManager.onScreen({
       _x: this._x,
       _y: this._y,
       _w: this._w,
       _h: this._h
       })) {

       }*!/

       if (Crafty.viewport.onScreen({_x: this._x, _y: this._y, _w: this._w, _h: this._h}) === true) {
       //console.log('On screen: ', this.inGameId);
       /!*          if (this.has('WebGL') === false) {
       //this.addComponent('WebGL');
       this.program.registerEntity(this);
       }*!/
       if (this.isRegistered === false) {
       this.program.registerEntity(this);
       this.isRegistered = true;
       }
       } else {
       if (this.isRegistered === true) {
       this.program.unregisterEntity(this);
       this.isRegistered = false;
       }
       /!*          if (this.has('WebGL')) {
       this.program.unregisterEntity(this);
       //this.removeComponent('WebGL');
       }*!/
       }

       /!*        if (((this._x >= -Crafty.viewport._x) && (this._x + this._w <= -Crafty.viewport._x + Crafty.viewport._width)) &&
       ((this._y >= -Crafty.viewport._y) && (this._y + this._h <= -Crafty.viewport._y + Crafty.viewport._height))) {
       //this.program.registerEntity(this);
       if (this.has('WebGL') === false) {
       this.addComponent('WebGL');
       this.program.registerEntity(this);
       }
       } else {
       //this.program.unregisterEntity(this);
       if (this.has('WebGL')) {
       this.program.unregisterEntity(this);
       this.removeComponent('WebGL');
       }
       }*!/
       });*/
    },
    setInGameId: function setInGameId() {
      this.inGameId =
        this.isoCoord.x.toString() + '.' +
        this.isoCoord.y.toString();
      this.addComponent(this.inGameId);
      //console.log(this.inGameId);
      return this;
    },
    setBlockType: function setBlockType(type) {
      if (this.blockType) {
        this.removeComponent(this.blockType);
        this.blockType = null;
      }

      let tempConfig = this.blocksConfig.testBlock[type];
      this.blockType = tempConfig.type;
      this.walkable = tempConfig.walkable;
      this.addComponent(tempConfig.blockType);
      Crafty.trigger(
        'PathfindModule:SetBlockWalkable',
        {
          isoCoord: this.isoCoord,
          value: this.walkable
        });
      /*switch (type) {
        case 'flat':
          this.blockType = this.blocksConfig.testBlock.flat.type;
          this.walkable = this.blocksConfig.testBlock.flat.walkable;
          this.addComponent(this.blockType);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'frontUp':
          this.blockType = this.blocksConfig.testBlock.frontUp.type;
          this.walkable = this.blocksConfig.testBlock.frontUp.walkable;
          this.addComponent(this.blockType);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'leftUp':
          this.blockType = this.blocksConfig.testBlock.leftUp.type;
          this.walkable = this.blocksConfig.testBlock.leftUp.walkable;
          this.addComponent(this.blockType);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'rightUp':
          this.blockType = type;
          this.walkable = 0;
          this.addComponent(type);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'backUp':
          this.blockType = type;
          this.walkable = 0;
          this.addComponent(type);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'frontLeftUp':
          this.blockType = type;
          this.walkable = 0;
          this.addComponent(type);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'frontRightUp':
          this.blockType = type;
          this.walkable = 0;
          this.addComponent(type);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'backLeftUp':
          this.blockType = type;
          this.walkable = 0;
          this.addComponent(type);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        case 'backRightUp':
          this.blockType = type;
          this.walkable = 0;
          this.addComponent(type);
          Crafty.trigger(
            'PathfindModule:SetBlockWalkable',
            {
              isoCoord: this.isoCoord,
              value: this.walkable
            });
          break;
        /!*        case 'rocks':
         //this.sprite(0, 2);
         this.blockType = 'rocks';
         this.addComponent('rocks');
         Crafty.trigger(
         'PathfindModule:SetBlockWalkable',
         {
         isoCoord: this.isoCoord,
         value: 1
         });
         break;*!/
      }*/
      return this;
    },
    place: function (x, y, z) {
      this.isoCoord = {
        x: x,
        y: y,
        z: z
      };
      return this;
    }
  });
};

var createGroundBlock = function createGroundBlock(msg) {
  'use strict';

  var tempBlock = Crafty.e('GroundBlock')
    .place(msg.x, msg.y, msg.z)
    .setInGameId()
    .setBlockType(msg.type);
  tempBlock.iso = msg.iso;
  tempBlock.iso.place(tempBlock, tempBlock.isoCoord.x, tempBlock.isoCoord.y, tempBlock.isoCoord.z);
  tempBlock.program.max_size = 1024 * 200;
  //console.log(tempBlock.program.max_size);
  //console.log(tempBlock.program);
  //tempBlock.program.unregisterEntity(tempBlock);
  //tempBlock.removeComponent('WebGL');
};

module.exports.createComponent = createComponent;
module.exports.createGroundBlock = createGroundBlock;


/*
 loadBlocksConfig()
 .then(function then(blocksConfig) {

 });*/

