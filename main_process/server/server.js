/**
 * Created by Redfox on 14.11.2015.
 */

const Koa = require('koa');
const IO = require('koa-socket');
const Jade = require('koa-jade');
const co = require('co');

const stylus = require('stylus');
const nib = require('nib');

const serve = require('koa-static');
const router = require('koa-router')();

const uuid = require('node-uuid');
const electron = require('electron');
const ipcMain  = electron.ipcMain ;
const BrowserWindow = require('browser-window');  // Module to create native browser window.

const koaApp = new Koa();
const io = new IO();

const jade = new Jade({
  viewPath: './main_process/server/views',
  debug: false,
  pretty: false,
  compileDebug: false
  //locals: global_locals_for_all_pages,
  /*  locals: {},
   basedir: 'path/for/jade/extends',
   helperPath: [
   'path/to/jade/helpers',
   {random: 'path/to/lib/random.js'},
   {_: require('lodash')}
   ]*/
});

// Attach the socket to the application 
io.attach(koaApp);

//jade.locals.someKey = 'some value';

var waitingClients = {};
var renderProcess;
ipcMain .on('WaitNewConnection', function(event, data) {
  console.log('WaitNewConnection', data);
  renderProcess = event.sender;
  waitingClients[data.uuid] = data.uuid;
});

router
  .get('/', function* (next) {
    //this.body = 'Hello World!';
    this.render('index', {}, true);
  })
  .get('/tests', function* (next) {
    this.render('tests', {}, true);
  })
  .get('/favicon.ico', function* (next) {
    console.log('GET favicon.ico');
  })
  .get('/:uuid', function* (next) {
    //this.body = 'Hello World!';
    var newUuid = this.params.uuid;

    console.log('###');
    console.log(this.params);
    console.log(newUuid);
    console.log(waitingClients);
    console.log('###');
    
    if(waitingClients[newUuid]) {
      this.render('uuid', {uuid: newUuid}, true);
      
      renderProcess.send('New extraClient', {
        uuid: newUuid
      });
      
    } else {
      this.body = 'Wrong ID';
    }
  });
/*  .post('/users', function *(next) {
 // ...
 })
 .put('/users/:id', function *(next) {
 // ...
 })
 .del('/users/:id', function *(next) {
 // ...
 });*/

const compile = function compile (str, path) {
  return stylus(str)
    .set('filename', path)
    .set('compress', false)
    .use(nib())
    .import('nib');
};

const stylusCompile = function stylusCompile(req, res, options){
  console.log(options);
  return function(callback){
    stylus.middleware(options)(req, res, callback);
  };
};

// middleware for koa
koaApp
  .use(jade.middleware)
  .use(router.routes())
  .use(router.allowedMethods())
  .use(serve('main_process/server/public'))
  .use(function*(next) {
    'use strict';
    //this.render('index', locals_for_this_page, true);
    yield stylusCompile(this.req, this.res, {
      src: __dirname + '/stylus',
      dest: __dirname + '/public',
      debug: true,
      force: true,
      compile: compile
    });
    yield next;
  });

var extraClients = {};

// middleware for socket.io's connect and disconnect
/*io.use(co.wrap(function* (ctx, next) {
  // on connect
  console.log('New connection');
  //console.log('headers', this.headers);
/!*  var extraClientUuid = uuid.v4();
  this.socket.emit('Connection:confirmed',
    {
      uuid: extraClientUuid
    });
  extraClients[extraClientUuid] = {
    uuid: extraClientUuid,
    socket: this.socket
  };
  var mainWindow = BrowserWindow.getFocusedWindow();
  console.log('webContents', mainWindow.webContents);
  mainWindow.webContents.send('New extraClient', {
    uuid: extraClientUuid
  });*!/
  this.socket.emit('Connection:confirmed');

  yield* next;

  // on disconnect
  console.log('Connection end');
}));*/

io.on('connection', (ctx, data) => {
  'use strict';
  console.log('New connection');
});

io.on('disconnect', (ctx, data) => {
  'use strict';
  console.log('Connection lost');
});

io.on('my other event', (ctx, data) => {
  console.log(`message: ${data}`);
});

//io.on('Add extra client', (ctx, data) => {
//  console.log(data);
//});

/*// router for socket event
koaApp.io
  .route('my other event', function* () {
  console.log(this.data);
  /!*  // we tell the client to execute 'new message'
   var message = this.args[0];
   this.broadcast.emit('new message', message);*!/
  })
  .route('Add extra client', function* () {
    console.log(this.data);
    //extraClients['']
  });*/

koaApp.server.listen(3000);
