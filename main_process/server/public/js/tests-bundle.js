(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by Redfox on 22.11.2015.
 */

const easing = require('easing');

(function () {

  var canvas = document.getElementById('canvas');
  canvas.width = 600;
  canvas.height = 300;
  var ctx = canvas.getContext('2d');
  var raf;

  var img = new Image();
  img.src = 'img/test.png';
  img.onload = function() {
    ctx.drawImage(img, 50, 50);
  };
  
  function init() {
    window.requestAnimationFrame(draw);
    ctx.globalCompositeOperation = 'screen';
  }


  var ballR = {
    x: 100,
    y: 100,
    vx: 5,
    vy: 2,
    radius: 25,
    color: '#e00000',
    draw: function draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
      ctx.closePath();
      ctx.fillStyle = this.color;
      ctx.fill();
    },
    move: function move() {
      this.x += this.vx;
      this.y += this.vy;
    },
    checkBorders: function checkBorders() {
      if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
        this.vy = -this.vy;
      }
      if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
        this.vx = -this.vx;
      }
    }
  };

  var ballG = {
    x: 100,
    y: 100,
    vx: -5,
    vy: 2,
    radius: 25,
    color: '#00e600',
    draw: function draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
      ctx.closePath();
      ctx.fillStyle = this.color;
      ctx.fill();
    },
    move: function move() {
      this.x += this.vx;
      this.y += this.vy;
    },
    checkBorders: function checkBorders() {
      if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
        this.vy = -this.vy;
      }
      if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
        this.vx = -this.vx;
      }
    }
  };

  var ballB = {
    x: 100,
    y: 100,
    vx: 2,
    vy: 2,
    radius: 25,
    color: '#0000df',
    draw: function draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
      ctx.closePath();
      ctx.fillStyle = this.color;
      ctx.fill();
    },
    move: function move() {
      this.x += this.vx;
      this.y += this.vy;
    },
    checkBorders: function checkBorders() {
      if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
        this.vy = -this.vy;
      }
      if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
        this.vx = -this.vx;
      }
    }
  };
  

  function draw() {

    ctx.clearRect(0,0, canvas.width, canvas.height);

    ctx.fillStyle = '#333333';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(img, 50, 50);
    
    ballR.draw();
    ballG.draw();
    ballB.draw();

    ballR.checkBorders();
    ballG.checkBorders();
    ballB.checkBorders();

    ballR.move();
    ballG.move();
    ballB.move();
    
    raf = window.requestAnimationFrame(draw);
    
   /* var time = new Date();


    ctx.clearRect(0, 0, canvas.width, canvas.height);
    
    ctx.fillStyle = '#000000';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    
    ctx.fillStyle = '#e00000';
    ctx.strokeStyle = '#e00000';
    //ctx.fillRect(20, 25, 100, 100);
    ctx.beginPath();
    ctx.moveTo(25 + time.getSeconds()/2, 20);
    ctx.lineTo(25, 100);
    ctx.lineTo(65, 100);
    ctx.closePath();
    ctx.fill();
    //ctx.stroke();

    ctx.fillStyle = '#00e600';
    ctx.strokeStyle = '#00e600';
    //ctx.fillRect(25, 25, 100, 100);
    ctx.beginPath();
    ctx.moveTo(30, 20);
    ctx.lineTo(30, 100);
    ctx.lineTo(70, 100);
    ctx.closePath();
    ctx.fill();
    //ctx.stroke();

    ctx.fillStyle = '#0000df';
    ctx.strokeStyle = '#0000df';
    //ctx.fillRect(25, 25, 100, 100);
    ctx.beginPath();
    ctx.moveTo(30, 20);
    ctx.lineTo(30, 100);
    ctx.lineTo(70, 100);
    ctx.closePath();
    ctx.fill();
    //ctx.stroke();

    
    window.requestAnimationFrame(draw);*/
    
  }

  init();

})();

},{"easing":2}],2:[function(require,module,exports){
module.exports = exports = Easing;

function Easing(list,type,options) {
    var funclist = {};
    var endToEnd = false;
    var invert = false;
    if (options !== undefined) {
        if ((options.endToEnd !== undefined) && (options.endToEnd === true)) {
            endToEnd = true; 
        }
        if ((options.invert !== undefined) && (options.invert === true)) {
            invert = true; 
        }
    }
    // you can call it with either Easing(11, 'linear') or Easing(new Array(11), 'linear')
    if (typeof list == 'number') {
        list = new Array(list);
    }
    funclist['linear'] = function(x) {
        return x;
    };
    funclist['quadratic'] = function(x) {
        return Math.pow(x,2);
    };
    funclist['cubic'] = function(x) {
        return Math.pow(x,3);
    };
    funclist['quartic'] = function(x) {
        return Math.pow(x,4);
    };
    funclist['quintic'] = function(x) {
        return Math.pow(x,5);
    };
    var sinusoidal = function(x) {
        return Math.sin(x*(Math.PI/2));
    }
    funclist['sinusoidal'] = sinusoidal;
    funclist['sin'] = sinusoidal;
    var exponential = function(x) {
        return Math.pow(2, 10 * (x - 1));
    };
    funclist['exponential'] = exponential;
    funclist['expo'] = exponential;
    funclist['exp'] = exponential;
    funclist['circular'] = function(x) {
        var time = (Math.PI*1.5) + (x * (Math.PI/2));
        return 1 + Math.sin(time);
    }
    if (type === undefined) {
        type = 'quadratic';
    }
    var step = 1/(list.length-1);

    for (var i = 1; i < list.length-1; i++) {
        list[i] = funclist[type](i*step);
    }
    list[0] = 0;
    list[list.length-1] = 1; 

    if (endToEnd) {
        var mid = Math.floor(list.length / 2);
        for (var i=1; i< mid; i++) {
            list[i] = list[i*2];
        }
        list[mid] = 1;
        for (var i=mid+1; i<list.length-1; i++) {
            list[i] = list[mid-(i-mid)];
        }
        list[list.length-1] = 0;
    }
    if (invert) {
        for (var i = 0; i < list.length;i++) {
            list[i] = 1 - list[i];
        }
    }
    return list;
};

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJtYWluX3Byb2Nlc3Mvc2VydmVyL3B1YmxpYy9qcy90ZXN0cy5qcyIsIm5vZGVfbW9kdWxlcy9lYXNpbmcvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2TEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8qKlxuICogQ3JlYXRlZCBieSBSZWRmb3ggb24gMjIuMTEuMjAxNS5cbiAqL1xuXG5jb25zdCBlYXNpbmcgPSByZXF1aXJlKCdlYXNpbmcnKTtcblxuKGZ1bmN0aW9uICgpIHtcblxuICB2YXIgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NhbnZhcycpO1xuICBjYW52YXMud2lkdGggPSA2MDA7XG4gIGNhbnZhcy5oZWlnaHQgPSAzMDA7XG4gIHZhciBjdHggPSBjYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtcbiAgdmFyIHJhZjtcblxuICB2YXIgaW1nID0gbmV3IEltYWdlKCk7XG4gIGltZy5zcmMgPSAnaW1nL3Rlc3QucG5nJztcbiAgaW1nLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgIGN0eC5kcmF3SW1hZ2UoaW1nLCA1MCwgNTApO1xuICB9O1xuICBcbiAgZnVuY3Rpb24gaW5pdCgpIHtcbiAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGRyYXcpO1xuICAgIGN0eC5nbG9iYWxDb21wb3NpdGVPcGVyYXRpb24gPSAnc2NyZWVuJztcbiAgfVxuXG5cbiAgdmFyIGJhbGxSID0ge1xuICAgIHg6IDEwMCxcbiAgICB5OiAxMDAsXG4gICAgdng6IDUsXG4gICAgdnk6IDIsXG4gICAgcmFkaXVzOiAyNSxcbiAgICBjb2xvcjogJyNlMDAwMDAnLFxuICAgIGRyYXc6IGZ1bmN0aW9uIGRyYXcoKSB7XG4gICAgICBjdHguYmVnaW5QYXRoKCk7XG4gICAgICBjdHguYXJjKHRoaXMueCwgdGhpcy55LCB0aGlzLnJhZGl1cywgMCwgTWF0aC5QSSoyLCB0cnVlKTtcbiAgICAgIGN0eC5jbG9zZVBhdGgoKTtcbiAgICAgIGN0eC5maWxsU3R5bGUgPSB0aGlzLmNvbG9yO1xuICAgICAgY3R4LmZpbGwoKTtcbiAgICB9LFxuICAgIG1vdmU6IGZ1bmN0aW9uIG1vdmUoKSB7XG4gICAgICB0aGlzLnggKz0gdGhpcy52eDtcbiAgICAgIHRoaXMueSArPSB0aGlzLnZ5O1xuICAgIH0sXG4gICAgY2hlY2tCb3JkZXJzOiBmdW5jdGlvbiBjaGVja0JvcmRlcnMoKSB7XG4gICAgICBpZiAodGhpcy55ICsgdGhpcy52eSA+IGNhbnZhcy5oZWlnaHQgfHwgdGhpcy55ICsgdGhpcy52eSA8IDApIHtcbiAgICAgICAgdGhpcy52eSA9IC10aGlzLnZ5O1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMueCArIHRoaXMudnggPiBjYW52YXMud2lkdGggfHwgdGhpcy54ICsgdGhpcy52eCA8IDApIHtcbiAgICAgICAgdGhpcy52eCA9IC10aGlzLnZ4O1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICB2YXIgYmFsbEcgPSB7XG4gICAgeDogMTAwLFxuICAgIHk6IDEwMCxcbiAgICB2eDogLTUsXG4gICAgdnk6IDIsXG4gICAgcmFkaXVzOiAyNSxcbiAgICBjb2xvcjogJyMwMGU2MDAnLFxuICAgIGRyYXc6IGZ1bmN0aW9uIGRyYXcoKSB7XG4gICAgICBjdHguYmVnaW5QYXRoKCk7XG4gICAgICBjdHguYXJjKHRoaXMueCwgdGhpcy55LCB0aGlzLnJhZGl1cywgMCwgTWF0aC5QSSoyLCB0cnVlKTtcbiAgICAgIGN0eC5jbG9zZVBhdGgoKTtcbiAgICAgIGN0eC5maWxsU3R5bGUgPSB0aGlzLmNvbG9yO1xuICAgICAgY3R4LmZpbGwoKTtcbiAgICB9LFxuICAgIG1vdmU6IGZ1bmN0aW9uIG1vdmUoKSB7XG4gICAgICB0aGlzLnggKz0gdGhpcy52eDtcbiAgICAgIHRoaXMueSArPSB0aGlzLnZ5O1xuICAgIH0sXG4gICAgY2hlY2tCb3JkZXJzOiBmdW5jdGlvbiBjaGVja0JvcmRlcnMoKSB7XG4gICAgICBpZiAodGhpcy55ICsgdGhpcy52eSA+IGNhbnZhcy5oZWlnaHQgfHwgdGhpcy55ICsgdGhpcy52eSA8IDApIHtcbiAgICAgICAgdGhpcy52eSA9IC10aGlzLnZ5O1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMueCArIHRoaXMudnggPiBjYW52YXMud2lkdGggfHwgdGhpcy54ICsgdGhpcy52eCA8IDApIHtcbiAgICAgICAgdGhpcy52eCA9IC10aGlzLnZ4O1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICB2YXIgYmFsbEIgPSB7XG4gICAgeDogMTAwLFxuICAgIHk6IDEwMCxcbiAgICB2eDogMixcbiAgICB2eTogMixcbiAgICByYWRpdXM6IDI1LFxuICAgIGNvbG9yOiAnIzAwMDBkZicsXG4gICAgZHJhdzogZnVuY3Rpb24gZHJhdygpIHtcbiAgICAgIGN0eC5iZWdpblBhdGgoKTtcbiAgICAgIGN0eC5hcmModGhpcy54LCB0aGlzLnksIHRoaXMucmFkaXVzLCAwLCBNYXRoLlBJKjIsIHRydWUpO1xuICAgICAgY3R4LmNsb3NlUGF0aCgpO1xuICAgICAgY3R4LmZpbGxTdHlsZSA9IHRoaXMuY29sb3I7XG4gICAgICBjdHguZmlsbCgpO1xuICAgIH0sXG4gICAgbW92ZTogZnVuY3Rpb24gbW92ZSgpIHtcbiAgICAgIHRoaXMueCArPSB0aGlzLnZ4O1xuICAgICAgdGhpcy55ICs9IHRoaXMudnk7XG4gICAgfSxcbiAgICBjaGVja0JvcmRlcnM6IGZ1bmN0aW9uIGNoZWNrQm9yZGVycygpIHtcbiAgICAgIGlmICh0aGlzLnkgKyB0aGlzLnZ5ID4gY2FudmFzLmhlaWdodCB8fCB0aGlzLnkgKyB0aGlzLnZ5IDwgMCkge1xuICAgICAgICB0aGlzLnZ5ID0gLXRoaXMudnk7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy54ICsgdGhpcy52eCA+IGNhbnZhcy53aWR0aCB8fCB0aGlzLnggKyB0aGlzLnZ4IDwgMCkge1xuICAgICAgICB0aGlzLnZ4ID0gLXRoaXMudng7XG4gICAgICB9XG4gICAgfVxuICB9O1xuICBcblxuICBmdW5jdGlvbiBkcmF3KCkge1xuXG4gICAgY3R4LmNsZWFyUmVjdCgwLDAsIGNhbnZhcy53aWR0aCwgY2FudmFzLmhlaWdodCk7XG5cbiAgICBjdHguZmlsbFN0eWxlID0gJyMzMzMzMzMnO1xuICAgIGN0eC5maWxsUmVjdCgwLCAwLCBjYW52YXMud2lkdGgsIGNhbnZhcy5oZWlnaHQpO1xuXG4gICAgY3R4LmRyYXdJbWFnZShpbWcsIDUwLCA1MCk7XG4gICAgXG4gICAgYmFsbFIuZHJhdygpO1xuICAgIGJhbGxHLmRyYXcoKTtcbiAgICBiYWxsQi5kcmF3KCk7XG5cbiAgICBiYWxsUi5jaGVja0JvcmRlcnMoKTtcbiAgICBiYWxsRy5jaGVja0JvcmRlcnMoKTtcbiAgICBiYWxsQi5jaGVja0JvcmRlcnMoKTtcblxuICAgIGJhbGxSLm1vdmUoKTtcbiAgICBiYWxsRy5tb3ZlKCk7XG4gICAgYmFsbEIubW92ZSgpO1xuICAgIFxuICAgIHJhZiA9IHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoZHJhdyk7XG4gICAgXG4gICAvKiB2YXIgdGltZSA9IG5ldyBEYXRlKCk7XG5cblxuICAgIGN0eC5jbGVhclJlY3QoMCwgMCwgY2FudmFzLndpZHRoLCBjYW52YXMuaGVpZ2h0KTtcbiAgICBcbiAgICBjdHguZmlsbFN0eWxlID0gJyMwMDAwMDAnO1xuICAgIGN0eC5maWxsUmVjdCgwLCAwLCBjYW52YXMud2lkdGgsIGNhbnZhcy5oZWlnaHQpO1xuICAgIFxuICAgIGN0eC5maWxsU3R5bGUgPSAnI2UwMDAwMCc7XG4gICAgY3R4LnN0cm9rZVN0eWxlID0gJyNlMDAwMDAnO1xuICAgIC8vY3R4LmZpbGxSZWN0KDIwLCAyNSwgMTAwLCAxMDApO1xuICAgIGN0eC5iZWdpblBhdGgoKTtcbiAgICBjdHgubW92ZVRvKDI1ICsgdGltZS5nZXRTZWNvbmRzKCkvMiwgMjApO1xuICAgIGN0eC5saW5lVG8oMjUsIDEwMCk7XG4gICAgY3R4LmxpbmVUbyg2NSwgMTAwKTtcbiAgICBjdHguY2xvc2VQYXRoKCk7XG4gICAgY3R4LmZpbGwoKTtcbiAgICAvL2N0eC5zdHJva2UoKTtcblxuICAgIGN0eC5maWxsU3R5bGUgPSAnIzAwZTYwMCc7XG4gICAgY3R4LnN0cm9rZVN0eWxlID0gJyMwMGU2MDAnO1xuICAgIC8vY3R4LmZpbGxSZWN0KDI1LCAyNSwgMTAwLCAxMDApO1xuICAgIGN0eC5iZWdpblBhdGgoKTtcbiAgICBjdHgubW92ZVRvKDMwLCAyMCk7XG4gICAgY3R4LmxpbmVUbygzMCwgMTAwKTtcbiAgICBjdHgubGluZVRvKDcwLCAxMDApO1xuICAgIGN0eC5jbG9zZVBhdGgoKTtcbiAgICBjdHguZmlsbCgpO1xuICAgIC8vY3R4LnN0cm9rZSgpO1xuXG4gICAgY3R4LmZpbGxTdHlsZSA9ICcjMDAwMGRmJztcbiAgICBjdHguc3Ryb2tlU3R5bGUgPSAnIzAwMDBkZic7XG4gICAgLy9jdHguZmlsbFJlY3QoMjUsIDI1LCAxMDAsIDEwMCk7XG4gICAgY3R4LmJlZ2luUGF0aCgpO1xuICAgIGN0eC5tb3ZlVG8oMzAsIDIwKTtcbiAgICBjdHgubGluZVRvKDMwLCAxMDApO1xuICAgIGN0eC5saW5lVG8oNzAsIDEwMCk7XG4gICAgY3R4LmNsb3NlUGF0aCgpO1xuICAgIGN0eC5maWxsKCk7XG4gICAgLy9jdHguc3Ryb2tlKCk7XG5cbiAgICBcbiAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGRyYXcpOyovXG4gICAgXG4gIH1cblxuICBpbml0KCk7XG5cbn0pKCk7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMgPSBFYXNpbmc7XG5cbmZ1bmN0aW9uIEVhc2luZyhsaXN0LHR5cGUsb3B0aW9ucykge1xuICAgIHZhciBmdW5jbGlzdCA9IHt9O1xuICAgIHZhciBlbmRUb0VuZCA9IGZhbHNlO1xuICAgIHZhciBpbnZlcnQgPSBmYWxzZTtcbiAgICBpZiAob3B0aW9ucyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGlmICgob3B0aW9ucy5lbmRUb0VuZCAhPT0gdW5kZWZpbmVkKSAmJiAob3B0aW9ucy5lbmRUb0VuZCA9PT0gdHJ1ZSkpIHtcbiAgICAgICAgICAgIGVuZFRvRW5kID0gdHJ1ZTsgXG4gICAgICAgIH1cbiAgICAgICAgaWYgKChvcHRpb25zLmludmVydCAhPT0gdW5kZWZpbmVkKSAmJiAob3B0aW9ucy5pbnZlcnQgPT09IHRydWUpKSB7XG4gICAgICAgICAgICBpbnZlcnQgPSB0cnVlOyBcbiAgICAgICAgfVxuICAgIH1cbiAgICAvLyB5b3UgY2FuIGNhbGwgaXQgd2l0aCBlaXRoZXIgRWFzaW5nKDExLCAnbGluZWFyJykgb3IgRWFzaW5nKG5ldyBBcnJheSgxMSksICdsaW5lYXInKVxuICAgIGlmICh0eXBlb2YgbGlzdCA9PSAnbnVtYmVyJykge1xuICAgICAgICBsaXN0ID0gbmV3IEFycmF5KGxpc3QpO1xuICAgIH1cbiAgICBmdW5jbGlzdFsnbGluZWFyJ10gPSBmdW5jdGlvbih4KSB7XG4gICAgICAgIHJldHVybiB4O1xuICAgIH07XG4gICAgZnVuY2xpc3RbJ3F1YWRyYXRpYyddID0gZnVuY3Rpb24oeCkge1xuICAgICAgICByZXR1cm4gTWF0aC5wb3coeCwyKTtcbiAgICB9O1xuICAgIGZ1bmNsaXN0WydjdWJpYyddID0gZnVuY3Rpb24oeCkge1xuICAgICAgICByZXR1cm4gTWF0aC5wb3coeCwzKTtcbiAgICB9O1xuICAgIGZ1bmNsaXN0WydxdWFydGljJ10gPSBmdW5jdGlvbih4KSB7XG4gICAgICAgIHJldHVybiBNYXRoLnBvdyh4LDQpO1xuICAgIH07XG4gICAgZnVuY2xpc3RbJ3F1aW50aWMnXSA9IGZ1bmN0aW9uKHgpIHtcbiAgICAgICAgcmV0dXJuIE1hdGgucG93KHgsNSk7XG4gICAgfTtcbiAgICB2YXIgc2ludXNvaWRhbCA9IGZ1bmN0aW9uKHgpIHtcbiAgICAgICAgcmV0dXJuIE1hdGguc2luKHgqKE1hdGguUEkvMikpO1xuICAgIH1cbiAgICBmdW5jbGlzdFsnc2ludXNvaWRhbCddID0gc2ludXNvaWRhbDtcbiAgICBmdW5jbGlzdFsnc2luJ10gPSBzaW51c29pZGFsO1xuICAgIHZhciBleHBvbmVudGlhbCA9IGZ1bmN0aW9uKHgpIHtcbiAgICAgICAgcmV0dXJuIE1hdGgucG93KDIsIDEwICogKHggLSAxKSk7XG4gICAgfTtcbiAgICBmdW5jbGlzdFsnZXhwb25lbnRpYWwnXSA9IGV4cG9uZW50aWFsO1xuICAgIGZ1bmNsaXN0WydleHBvJ10gPSBleHBvbmVudGlhbDtcbiAgICBmdW5jbGlzdFsnZXhwJ10gPSBleHBvbmVudGlhbDtcbiAgICBmdW5jbGlzdFsnY2lyY3VsYXInXSA9IGZ1bmN0aW9uKHgpIHtcbiAgICAgICAgdmFyIHRpbWUgPSAoTWF0aC5QSSoxLjUpICsgKHggKiAoTWF0aC5QSS8yKSk7XG4gICAgICAgIHJldHVybiAxICsgTWF0aC5zaW4odGltZSk7XG4gICAgfVxuICAgIGlmICh0eXBlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgdHlwZSA9ICdxdWFkcmF0aWMnO1xuICAgIH1cbiAgICB2YXIgc3RlcCA9IDEvKGxpc3QubGVuZ3RoLTEpO1xuXG4gICAgZm9yICh2YXIgaSA9IDE7IGkgPCBsaXN0Lmxlbmd0aC0xOyBpKyspIHtcbiAgICAgICAgbGlzdFtpXSA9IGZ1bmNsaXN0W3R5cGVdKGkqc3RlcCk7XG4gICAgfVxuICAgIGxpc3RbMF0gPSAwO1xuICAgIGxpc3RbbGlzdC5sZW5ndGgtMV0gPSAxOyBcblxuICAgIGlmIChlbmRUb0VuZCkge1xuICAgICAgICB2YXIgbWlkID0gTWF0aC5mbG9vcihsaXN0Lmxlbmd0aCAvIDIpO1xuICAgICAgICBmb3IgKHZhciBpPTE7IGk8IG1pZDsgaSsrKSB7XG4gICAgICAgICAgICBsaXN0W2ldID0gbGlzdFtpKjJdO1xuICAgICAgICB9XG4gICAgICAgIGxpc3RbbWlkXSA9IDE7XG4gICAgICAgIGZvciAodmFyIGk9bWlkKzE7IGk8bGlzdC5sZW5ndGgtMTsgaSsrKSB7XG4gICAgICAgICAgICBsaXN0W2ldID0gbGlzdFttaWQtKGktbWlkKV07XG4gICAgICAgIH1cbiAgICAgICAgbGlzdFtsaXN0Lmxlbmd0aC0xXSA9IDA7XG4gICAgfVxuICAgIGlmIChpbnZlcnQpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDtpKyspIHtcbiAgICAgICAgICAgIGxpc3RbaV0gPSAxIC0gbGlzdFtpXTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gbGlzdDtcbn07XG4iXX0=
