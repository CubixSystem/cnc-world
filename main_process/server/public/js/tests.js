/**
 * Created by Redfox on 22.11.2015.
 */

const easing = require('easing');

(function () {

  var canvas = document.getElementById('canvas');
  canvas.width = 600;
  canvas.height = 300;
  var ctx = canvas.getContext('2d');
  var raf;

  var img = new Image();
  img.src = 'img/test.png';
  img.onload = function() {
    ctx.drawImage(img, 50, 50);
  };
  
  function init() {
    window.requestAnimationFrame(draw);
    ctx.globalCompositeOperation = 'screen';
  }


  var ballR = {
    x: 100,
    y: 100,
    vx: 5,
    vy: 2,
    radius: 25,
    color: '#e00000',
    draw: function draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
      ctx.closePath();
      ctx.fillStyle = this.color;
      ctx.fill();
    },
    move: function move() {
      this.x += this.vx;
      this.y += this.vy;
    },
    checkBorders: function checkBorders() {
      if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
        this.vy = -this.vy;
      }
      if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
        this.vx = -this.vx;
      }
    }
  };

  var ballG = {
    x: 100,
    y: 100,
    vx: -5,
    vy: 2,
    radius: 25,
    color: '#00e600',
    draw: function draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
      ctx.closePath();
      ctx.fillStyle = this.color;
      ctx.fill();
    },
    move: function move() {
      this.x += this.vx;
      this.y += this.vy;
    },
    checkBorders: function checkBorders() {
      if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
        this.vy = -this.vy;
      }
      if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
        this.vx = -this.vx;
      }
    }
  };

  var ballB = {
    x: 100,
    y: 100,
    vx: 2,
    vy: 2,
    radius: 25,
    color: '#0000df',
    draw: function draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
      ctx.closePath();
      ctx.fillStyle = this.color;
      ctx.fill();
    },
    move: function move() {
      this.x += this.vx;
      this.y += this.vy;
    },
    checkBorders: function checkBorders() {
      if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
        this.vy = -this.vy;
      }
      if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
        this.vx = -this.vx;
      }
    }
  };
  

  function draw() {

    ctx.clearRect(0,0, canvas.width, canvas.height);

    ctx.fillStyle = '#333333';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(img, 50, 50);
    
    ballR.draw();
    ballG.draw();
    ballB.draw();

    ballR.checkBorders();
    ballG.checkBorders();
    ballB.checkBorders();

    ballR.move();
    ballG.move();
    ballB.move();
    
    raf = window.requestAnimationFrame(draw);
    
   /* var time = new Date();


    ctx.clearRect(0, 0, canvas.width, canvas.height);
    
    ctx.fillStyle = '#000000';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    
    ctx.fillStyle = '#e00000';
    ctx.strokeStyle = '#e00000';
    //ctx.fillRect(20, 25, 100, 100);
    ctx.beginPath();
    ctx.moveTo(25 + time.getSeconds()/2, 20);
    ctx.lineTo(25, 100);
    ctx.lineTo(65, 100);
    ctx.closePath();
    ctx.fill();
    //ctx.stroke();

    ctx.fillStyle = '#00e600';
    ctx.strokeStyle = '#00e600';
    //ctx.fillRect(25, 25, 100, 100);
    ctx.beginPath();
    ctx.moveTo(30, 20);
    ctx.lineTo(30, 100);
    ctx.lineTo(70, 100);
    ctx.closePath();
    ctx.fill();
    //ctx.stroke();

    ctx.fillStyle = '#0000df';
    ctx.strokeStyle = '#0000df';
    //ctx.fillRect(25, 25, 100, 100);
    ctx.beginPath();
    ctx.moveTo(30, 20);
    ctx.lineTo(30, 100);
    ctx.lineTo(70, 100);
    ctx.closePath();
    ctx.fill();
    //ctx.stroke();

    
    window.requestAnimationFrame(draw);*/
    
  }

  init();

})();
