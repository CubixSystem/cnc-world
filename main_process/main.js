/**
 * Created by Redfox on 04.10.2015.
 */

const fs = require('fs');

const app = require('app');  // Module to control application life.
const BrowserWindow = require('browser-window');  // Module to create native browser window.

const electron = require('electron');
const ipcMain  = electron.ipcMain;

/*// Report crashes to our server.
require('crash-reporter').start();*/

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function windowAllClosed() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform != 'darwin') {
    app.quit();
  }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function ready() {
  'use strict';
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    'auto-hide-menu-bar': true
  });

  //mainWindow.setFullScreen(true);

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/index.html');

  // Open the DevTools.
  mainWindow.openDevTools();

  let loadBlocksConfig = function loadBlocksConfig() {
    return new Promise(function promise(resolve, reject) {
      'use strict';
      let blocksConfigPath = './main_process/configs/ground-block.json';
      fs.readFile(blocksConfigPath, 'utf8', function readFile(err, data) {
        if (err){
          reject(err);
          throw err; // we'll not consider error handling for now
        }
        let obj = JSON.parse(data);
        resolve(obj);
      });
    });
  };

  loadBlocksConfig()
    .then(function then(blocksConfig) {

      ipcMain.on('blocksConfig-message', (event, data) => {
        event.sender.send('blocksConfig-reply', blocksConfig);
      });

      /*    let window = mainWindow.getFocusedWindow();
       window.webContents.send('blocksConfig', blocksConfig);*/

      //window.webContents.on('did-finish-load', function() {
      //  window.webContents.send('blocksConfig', blocksConfig);
      //});
    });

/*  ipcMain .on('WorldId', function(event, data) {
    console.log('WorldId', data);
  });*/



/*  mainWindow.webContents.on('did-finish-load', function() {
    'use strict';
    console.log('did-finish-load');

  });*/
  

  // Emitted when the window is closed.
  mainWindow.on('closed', function closed() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  require('./server/server');
  
});

/*const ipcMain  = require('ipcMain ');

ipcMain .on('asynchronous-message', function(event, arg) {
  console.log(arg);  // prints "ping"
  event.sender.send('asynchronous-reply', 'pong');
});*/


